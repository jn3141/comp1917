/* This is a program written in C to determine whether or not a number is odd or even.
   By: Justin Ng
   Date: 2/03/2016
*/

// Loading headers
#include <stdio.h>
#include <stdlib.h>

// Setting up the function
int main(int argc, char ** argv) {
   int x;
   printf("What number do you want to check?\n");
   scanf("%d",&x);
   if (x % 2 == 1) {
      printf("This is odd.\n");
   } else if (x % 2 == 0) {
      printf("This is even.\n");
   } else {
      printf("This is not even a number!\n");
   }
   return EXIT_SUCCESS;
}
