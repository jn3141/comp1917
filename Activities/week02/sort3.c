// Justin Ng (z5116690)
// wed13-oboe, tutor: Bernice Chen
// 15/03/2016
// COMP1917
// Takes 3 whole numbers and orders them in ascending order.
// Not allowed to use any syntax beyond if statements
// Assume all distinct numbers

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    int first, second, third;
    scanf ("%d %d %d", &first, &second, &third);
	if (first > second && second > third) {
	    printf("%d\n", third);
		printf("%d\n", second);
		printf("%d\n", first);
	} else if (first > third && third > second) {
	    printf("%d\n", second);
		printf("%d\n", third);
		printf("%d\n", first);
	} else if (second > first && first > third) {
	    printf("%d\n", third);
		printf("%d\n", first);
		printf("%d\n", second);
	} else if (second > third && third > first) {
	    printf("%d\n", first);
		printf("%d\n", third);
		printf("%d\n", second);
	} else if (third > first && first > second) {
	    printf("%d\n", second);
		printf("%d\n", first);
		printf("%d\n", third);
	} else if (third > second && second > first) {
	    printf("%d\n", first);
		printf("%d\n", second);
		printf("%d\n", third);
	}
	return EXIT_SUCCESS;
}
