// Justin Ng and Jamison Tsai
// wed13-oboe, tutor: Bernice Chen
// 16/03/2016
// COMP1917
// used to investigate where the C compiler stores variables and
//   how much memory it uses for different types

//
//  main.c
//  memory
//
//  Created by Richard Buckland on 20/11/12.
//

#include <stdio.h>
#include <stdlib.h>

long add (int x, int y);

int main(int argc, const char * argv[]) {

   int x;
   int y;
   long total;

   x = 40;
   y = 2;

   total = add (x, y);

   printf("the sum of %d and %d is %ld\n", x, y, total);

   char c ='a';
/*
   unsigned long ul       = 4294967295LL;
   unsigned int ui        = 0;
   unsigned long long ull = 0;
   unsigned short us      = 0;

   signed long sl       = 0;
   signed int si        = 0;
   signed long long sll = 0;
   signed short ss      = 32767;

   long l       = 0;
   int i        = 0;
   long long ll = 0;
   short s      = 0;

   float f = 0;
   double d = 0;
*/
   unsigned long ul       = -1;
   unsigned int ui        = 4294967295u;
   unsigned long long ull = 18446744073709551615uLL;
   unsigned short us      = 65535u;

   signed long sl       = 2147483647L;
   signed int si        = 2147483647;
   signed long long sll = 9223372036854775807LL;
   signed short ss      = 32767;

   long l       = 4294967295LL;
   int i        = 4294967295LL;
   long long ll = -5;
   short s      = 65535;

   float f = 3.1;
   double d = 3.14;
   // add them all together just to make use of them so the compiler
   // doesn't grumble that they are unused in the program

   double grandTotal;
   grandTotal =  c +
                 ul + ui + ull + us +
                 sl + si + sll + ss +
                  l +  i +  ll +  s +
                  f + d;

   printf ("all these things added together make %f\n", grandTotal);
   printf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
      sizeof(ul), sizeof(ui), sizeof(ull), sizeof(us), sizeof(sl),
      sizeof(si), sizeof(sll), sizeof(ss), sizeof(l), sizeof(i),
      sizeof(ll), sizeof(s), sizeof(f), sizeof(d));
   printf("%p, %p, %p, %p, %p, %p, %p, %p, %p, %p, %p, %p, %p, %p\n",
      &ul, &ui , &ull, &us, &sl, &si, &sll, &ss, &l, &i, &ll, &s, &f, &d);
   printf("%lu, %u, %Lu, %d, %ld, %d, %Ld, %d, %ld, %d, %Ld, %d, %f, %f\n",
      ul, ui, ull, us, sl, si, sll, ss, l, i, ll, s, f, d);
   return EXIT_SUCCESS;
}

long add (int x, int y) {
   long answer;
   answer = x + y;

   return answer;
}
