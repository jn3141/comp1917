// Justin Ng (z5116690)
// Tutor class is wed13-oboe
// Tutor is Bernice Chen
// File first created on the 23/03/2016
// This programs uses a function and 'while' loops to print the Danish Flag.

#include <stdio.h>
#include <stdlib.h>

void showDanish(void);

int main(int argc, char *argv[]) {
    showDanish();
    return EXIT_SUCCESS;
}

void showDanish(void) {
    int row = 0;
    while (row < 5) {
        int column = 0;
        while (column < 12) {
            if (row == 2 || column == 2) {
                printf(" ");
            } else {
                printf("*");
            }
            if (column == 11) {
                printf("\n");
            }
            column += 1;
        }
        row += 1;
    }
}
