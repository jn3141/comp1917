// Justin Ng & Jeff Zhang
// Tutor class is wed13-oboe
// Tutor is Bernice Chen
// File first created on the 23/03/2016
// This programs swaps the addresses of two variables.

#include <stdio.h>
#include <stdlib.h>

void swap(int *first, int *second);

int main(int argc, char *argv[]){
	int first, second;
	first = 1;
	second = 2;
	
	swap(&first, &second);
	
	printf("first: %d, second:%d\n", first, second);
	
	return EXIT_SUCCESS;
}

void swap(int *first, int *second){
	int temp = *first;
	*first = *second;
	*second = temp;
}