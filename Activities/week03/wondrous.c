// Justin Ng (z5116690)
// Tutor class is wed13-oboe
// Tutor is Bernice Chen
// File first created on the 23/03/2016
// This programs runs the 'Wondrous function'
// In this function:
// If the number is even, it is halved
// If the number is odd, it is multiplied by 3, then has 1 added.

#include <stdio.h>
#include <stdlib.h>

int printWondrous(int start);

int main(int argc, char *argv[]) {
    int start = 0;
    scanf("%d", &start);
    printWondrous(start);
    return EXIT_SUCCESS;
}

int printWondrous(int start) {
    int counter = 1;
    printf("%d ", start);
    while (start != 1) {
        if (start % 2 == 0) {
            start = start / 2;
        } else {
            start = (start * 3) + 1;
        }
        printf("%d ", start);
        counter += 1;
    }
    printf("\n");
    return counter;
}
