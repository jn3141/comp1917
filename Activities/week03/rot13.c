// Justin Ng & Jeff Zhang
// Tutor class is wed13-oboe
// Tutor is Bernice Chen
// File first created on the 23/03/2016
// This program encodes a character as 13 characters further in the alphabet.
// If it goes beyond the letter 'z', it begins again from 'a'.

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

char encode(char letter);

int main(int argc, char*argv[]){
    char letter;
    char encoded = (scanf("%c", &letter));
    encode(encoded);
    printf("the new character is %c\n", encoded);
    return EXIT_SUCCESS;
}

char encode (char letter){
	if(letter >= 'a' && letter <= 'z'){
		letter = (((letter - 97) + 13) % 26) + 97;
	} else if (letter >= 'A' && letter <= 'Z') {
		letter = (((letter - 65) + 13) % 26) + 65;
	}
	
	return letter;
}

