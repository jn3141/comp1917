// Justin Ng, justinn, z5116690
// wed13-oboe, tutored by Bernice Chen
// Date: 6/03/2016
// This program displays a Danish Flag, the flag of the amazing Danes

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {
    printf("#####  ############\n");
    printf("#####  ############\n");
    printf("#####  ############\n");
    printf("                   \n");
    printf("#####  ############\n");
    printf("#####  ############\n");
    printf("#####  ############\n");
    return EXIT_SUCCESS;
}
