// Justin Ng, justinn, z5116690
// wed13-oboe, tutored by Bernice Chen
// Date: 6/03/2016
// This program displays a majestic dragon,
// or rather something opposite of majestic.

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {
    printf("      __()\n");
    printf(" @---^  `/\n");
    printf("|-----_  `/            '\n");
    printf("       \\  \\`         '   '\n");
    printf("       /  /           ' '\n");
    printf("      /  /  _____   ' @  '\n");
    printf("     /  /  /  _  \\   / \\\n");
    printf("     \\  \\_/  / \\  \\_/  /\n");
    printf("      \\_____/   \\_____/\n");
    return EXIT_SUCCESS;
}
