// Jamison Tsai, Justin Ng
// 9/3/2016
// COMP1917
// test if it is a leap year

/*
    Year 	is leap year?
    4000 	yes
    4004 	yes
    1999 	no
    1900 	no
    2000 	yes
    1904 	yes
    1581	no
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define START_GREGORIAN_CALENDAR 1582

int isLeapYear (int year) {

	if (year % 4 != 0) {
		return 0;
	} else {
		if (year % 100 != 0) {
			return 1 ;
		} else {
			if (year % 400 != 0) {
				return 0;
			} else {
				return 1;
			}
		}
	}
}

int main (int argc, char* argv[]) {
	int year;
	printf("please enter the year you are interested in\n");
	scanf("%d", &year);
	assert(year >= START_GREGORIAN_CALENDAR);
	int ans = isLeapYear(year);
	if (ans == 1) {
		printf("%d is a leap year!\n", year);
	} else {
		printf("%d is not a leap year!\n", year);
	}
	return EXIT_SUCCESS;
}
