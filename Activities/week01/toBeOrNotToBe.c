// Justin Ng, justinn, z5116690
// wed13-oboe, tutored by Bernice Chen
// Date: 6/03/2016
// This is a program that asks the age old question: To be, or not to be?

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char * argv[]) {
    printf("To be, or not to be?\n");
    return EXIT_SUCCESS;
}
