// Justin Ng & Jeff Zhang
// Tutor class is wed13-oboe
// Tutor is Steven Strijakov
// File first created on the 13/04/2016
// This program is used to analyse arrays.

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char * argv[]) {
    int canaryA = 42;
    int testArray[10] = {0,1,2,3,4,5,6,7,8,9};
    int canaryB = 99;

    int index = -2;
    printf ("The address of canaryA is %p.\n",&canaryA);
    printf ("The address of canaryB is %p.\n",&canaryB);
    while (index < 12) {
        printf ("testArray[%d]=%p,\n",index, &testArray[index]);
        index++;
    }
    return EXIT_SUCCESS;
}