
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#define DIFFERENCE 32

int main(int argc, char* argv[]){
	char c;
	int letterLen = 0;
    int charLen = 0;
    int LFLen = 0;
    int spaceLen = 0;
	int charCount[255] = {0};

	while((c = getchar()) != EOF) {
		printf("%c",c);
		charLen++;
        if (c >= 'A' && c <= 'Z') {
		    c = c + DIFFERENCE;
		    letterLen++;
	    } else if (c >= 'a' && c <= 'z') {
	        letterLen++;
		} else if (c == 10) {
            LFLen++;
        } else if (c == ' ') {
            spaceLen++;
        }
		charCount[(int)c]++;
    }
 	printf("The statistics of the file are:\n");
 	int pos = 'a';
    printf("There are %d characters in total.\n", charLen);
 	printf("There are %d letters in total.\n",letterLen);
    printf("There are %d LF's in total.\n",LFLen);
    printf("There are %d spaces in total.\n",spaceLen);
    while (pos <='z') {
 	    double frequency = (charCount[pos]*100.0)/letterLen;
        printf("Character %c had count %d, with the frequency being %f%%\n", pos, charCount[pos], frequency);
 		pos++;
 	}

    return EXIT_SUCCESS;
}
