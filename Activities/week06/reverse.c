// Justin Ng & Jeff Zhang
// Tutor class is wed13-oboe
// Tutor is Steven Strijakov
// File first created on the 20/04/2016
// This program takes a string... and reverses it

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* reverse (char* str);

int main (int argc, char* argv[]) {
    char string[255] = "This is a string";
    printf("The string is:\n");
    printf("%s\n", string);
    printf("The length of this string is %lu.\n\n", strlen(string));
    reverse(string);
    printf("The reversed string is:\n");
    printf("%s\n", string);
    printf("The length of this string is %lu.\n", strlen(string));
    return EXIT_SUCCESS;

}

char* reverse (char* message) {

    int strLen = strlen(message);
    int curPos = strLen - 1;
    int otherPos = 0;\
    char* revString = malloc(strLen + 1);

    // load the message into revString backwards
    if (message[0] != '\0') {
        while (curPos >= 0) {
            revString[otherPos] = message[curPos];
            otherPos++;
            curPos--;
        }
    }
    revString[strLen] = '\0';
    int newPos = 0;

    // write the revString back into message
    while (newPos <= strLen) {
        message[newPos] = revString[newPos];
        newPos++;
    }

    return revString;
}
