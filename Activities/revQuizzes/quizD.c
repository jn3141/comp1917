#include <stdio.h>
#include <stdlib.h>

void quizD (void);

int main(int argc, char* argv[]) {
    quizD();
    return EXIT_SUCCESS;
}

void quizD (void) {

   char *a = "mandelbrot";
   char *b = "set";
   char *c = "message!";
   char d[] = "message!";
   char *z = "message!";

   char sequence = 'a';
   printf ("\nQuiz D - arrays and strings\n");

   printf ("%c) %u   \t", sequence++,  (int) (a==b));
   printf ("%c) %u   \t", sequence++,  (int) (a==c));
   printf ("%c) %u\n"   , sequence++,  (int) (a==d));

   printf ("%c) %u   \t", sequence++,  (int) (c==a));
   printf ("%c) %u   \t", sequence++,  (int) (c==c));
   printf ("%c) %u   \t", sequence++,  (int) (c==d));
   printf ("%c) %u\n"   , sequence++,  (int) (c==z));

   printf ("%c) %u   \t", sequence++,  (int) sizeof (a));
   printf ("%c) %u   \t", sequence++,  (int) sizeof (b));
   printf ("%c) %u   \t", sequence++,  (int) sizeof (c));
   printf ("%c) %u\n"   , sequence++,  (int) sizeof (d));

   printf ("%c) %u   \t", sequence++,  (int) (c));
   printf ("%c) %u   \t", sequence++,  (int) (d));

   //dereferencing
   printf ("%c) %u   \t", sequence++,  (int) (*c == *a));
   printf ("%c) %u   \t", sequence++,  (int) (*c == *b));
   printf ("%c) %u   \t", sequence++,  (int) (*c == *c));
   printf ("%c) %u   \t", sequence++,  (int) (*c == *d));
   printf ("%c) %u\n"   , sequence++,  (int) (*c == *z));

}
