// revision quiz C
// predict what will be printed by each line and then run the program to confirm

   #include <stdio.h>
   #include <stdlib.h>
   #include <assert.h>

    void quizC (void);

    int main (int argc, char *argv[]) {
       quizC ();
       printf ("quiz done!\n");
       return EXIT_SUCCESS;
    }

    void quizC (void) {
       short a;
       short *b;
       short *c;

       a = 42;
       b = &a;
       c = b;

       char sequence = 'a';
       printf ("\nQuiz C - dereferencing\n");

       printf ("%c) %u   \t", sequence++,  (int) &a);
       printf ("%c) %u   \t", sequence++,  (int) &b);
       printf ("%c) %u\n"   , sequence++,  (int) &c);

       printf ("%c) %u   \t", sequence++,  (int) a);
       printf ("%c) %u   \t", sequence++,  (int) b);
       printf ("%c) %u\n"   , sequence++,  (int) c);

       printf ("%c) %u   \t", sequence++,  (int) (*b));
       printf ("%c) %u   \t", sequence++,  (int) (*c));
       printf ("%c) %u\n"   , sequence++,  (int) (*(&a)));

       printf ("\n");
       short d;
       d = a;

       printf ("%c) %u   \t", sequence++,  ((int) a== (int) b));
       printf ("%c) %u   \t", sequence++,  ((int) a== (int) c));
       printf ("%c) %u\n"   , sequence++,  (a==d));

       printf ("%c) %u   \t", sequence++,  (b==c));
       printf ("%c) %u   \t", sequence++,  ((*b)==(*c)));
       printf ("%c) %u\n"   , sequence++,  (d==(*b)));

       printf ("%c) %u   \t", sequence++,  (&b==&c));
       printf ("%c) %u   \t", sequence++,  (&a==b));
       printf ("%c) %u\n"   , sequence++,  ((int) &a) == ((int) &b));

       printf ("%c) %u\n"   , sequence++,  (int) (&a==&d));

 }
