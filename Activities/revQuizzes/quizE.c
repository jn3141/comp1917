void arrayTest (char buffer[]) {
   // print the size of the input
   printf ("%d\n", (int) sizeof buffer);
}

void charPointerTest (char *buffer) {
   // print the size of the input
   printf ("%d\n", (int) sizeof buffer);
}


void quizE (void) {

   char *a = "mandelbrot";
   char *b = "set";
   char *c = "message!";
   char d[] = "message!";

   printf ("\nQuiz E - more arrays and strings\n");

   arrayTest (a);
   arrayTest (b);
   arrayTest (c);
   arrayTest (d);
   printf ("\n");

   charPointerTest (a);
   charPointerTest (b);
   charPointerTest (c);
   charPointerTest (d);

}
