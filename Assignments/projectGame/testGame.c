/*
 *  testGame.c
 *  GROUP JJJS
 *  Jeff Zhang, Justin Ng, Jonathan Wong, Shashika Rupasinghe
 *  Wednesday Tutorial
 *  Unit tests for Game.c
 */


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Game.h"

/*#define CYAN STUDENT_BQN
#define PURP STUDENT_MMONEY
#define YELL STUDENT_MJ
#define RED STUDENT_BPS
#define GREE STUDENT_MTV
#define BLUE STUDENT_THD
#define TRUE 1
#define FALSE 0
*/

#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, \
                STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, \
                STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, \
                STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}
#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

//#define DEFAULT_DISCIPLINES {CYAN,PURP,YELL,PURP,YELL,RED ,GREE,GREE,
//RED,GREE,CYAN,YELL,CYAN,BLUE,YELL,PURP,GREE,CYAN,RED }
//#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,9,9,2,8,10,5}

void testgetTurnNumber (void);
void testgetWhoseTurn (void);
void testgetKPIpoints(void);
void testgetARCs(void);
void testgetGO8s(void);
void testgetCampuses(void);
void testgetIPs(void);
void testgetPublications(void);
void testgetMostARCs(void);
void testgetMostPublications(void);
void testgetStudents(void);
void testgetExchangeRate(void);

int main (int argc, const char * argv[]) {
   printf ("Running testGame.c with JJJS's tests\n");
   testgetTurnNumber ();
   testgetWhoseTurn();
   testgetKPIpoints();
   testgetARCs();
   testgetGO8s();
   testgetCampuses();
   testgetIPs();
   testgetPublications();
   testgetMostARCs();
   testgetMostPublications();
   testgetStudents();
   testgetExchangeRate();
   printf ("All tests passed! You are awesome!\n");
   return EXIT_SUCCESS;
}

void testgetDiscipline (void) {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    assert(getDiscipline(g, 1)==STUDENT_MMONEY);
    assert(getDiscipline(g, 2)==STUDENT_MJ);
    assert(getDiscipline(g, 3)==STUDENT_MMONEY);
    assert(getDiscipline(g, 4)==STUDENT_MJ);
    assert(getDiscipline(g, 5)==STUDENT_BPS);
    assert(getDiscipline(g, 6)==STUDENT_MTV);
    assert(getDiscipline(g, 7)==STUDENT_MTV);
    assert(getDiscipline(g, 8)==STUDENT_BPS);
    assert(getDiscipline(g, 9)==STUDENT_MTV);
    assert(getDiscipline(g, 10)==STUDENT_BQN);
    assert(getDiscipline(g, 11)==STUDENT_MJ);
    assert(getDiscipline(g, 12)==STUDENT_BQN);
    assert(getDiscipline(g, 13)==STUDENT_THD);
    assert(getDiscipline(g, 14)==STUDENT_MJ);
    assert(getDiscipline(g, 15)==STUDENT_MMONEY);
    assert(getDiscipline(g, 16)==STUDENT_MTV);
    assert(getDiscipline(g, 17)==STUDENT_BQN);
    assert(getDiscipline(g, 18)==STUDENT_BPS);
    printf("getDiscipline:PASSED\n");
}

void testgetTurnNumber (void) {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    printf("testing getTurnNumber...");
    assert(getTurnNumber(g) == -1);
    printf("Passed\n");
}

void testgetDiceValue (void) { ///////////////////////////////////////////////////////
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    int regionID = 0;
    int diceValue[19] = DEFAULT_DICE;

    while(regionID <= 19){
        assert(getDiceValue(g, regionID) == diceValue[regionID]);
        regionID++;
    }

    disposeGame (g);
}

void testgetMostARCs(void) {
   printf("testing getMostARCs\n");


   int disciplines[] = DEFAULT_DISCIPLINES;
   int dice[] = DEFAULT_DICE;
   Game g = newGame (disciplines, dice);

   assert(getTurnNumber(g) == -1);
   assert(getMostARCs(g) == NO_ONE);

}


void testgetMostPublications(void) {
   printf("testing getMostPublications\n");


   int disciplines[] = DEFAULT_DISCIPLINES;
   int dice[] = DEFAULT_DICE;
   Game g = newGame (disciplines, dice);

   assert(getTurnNumber(g) == -1);
   assert(getMostPublications(g) == NO_ONE);

}

void testgetWhoseTurn (void) {
   printf("Testing getWhoseTurn\n");

   int disciplines[] = DEFAULT_DISCIPLINES;
   int dice[] = DEFAULT_DICE;
   Game g = newGame (disciplines, dice);

   assert(getWhoseTurn(g) == UNI_C);
   //incremeant turn number
   // does anyone know why it doesnt work when i try to increment turn number by saying g->currentTurn = -1;
   throwDice (g, 1);
   assert(getWhoseTurn(g) == UNI_A);
   throwDice (g, 1);
   assert(getWhoseTurn(g) == UNI_B);
   throwDice (g, 1);
   assert(getWhoseTurn(g) == UNI_C);
   throwDice (g, 1);
   assert(getWhoseTurn(g) == UNI_A);
}

void testisLegalAction(void) {
    printf("Testing the 'isLegalAction' function");

    // sets up a new game first of all
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    // makes an action before the game starts, which is illegal
    action testTerraNullis;
    testTerraNullis.actionCode = PASS;
    assert(isLegalAction(g,testTerraNullis) == FALSE);
    testTerraNullis.actionCode = BUILD_CAMPUS;
    assert(isLegalAction(g,testTerraNullis) == FALSE);
    testTerraNullis.actionCode = BUILD_GO8;
    assert(isLegalAction(g,testTerraNullis) == FALSE);
    testTerraNullis.actionCode = OBTAIN_ARC;
    assert(isLegalAction(g,testTerraNullis) == FALSE);
    testTerraNullis.actionCode = START_SPINOFF;
    assert(isLegalAction(g,testTerraNullis) == FALSE);
    testTerraNullis.actionCode = OBTAIN_PUBLICATION;
    assert(isLegalAction(g,testTerraNullis) == FALSE);
    testTerraNullis.actionCode = OBTAIN_IP_PATENT;
    assert(isLegalAction(g,testTerraNullis) == FALSE);
    testTerraNullis.actionCode = RETRAIN_STUDENTS;
    assert(isLegalAction(g,testTerraNullis) == FALSE);

    // take a first turn
    throwDice (g, 2);

    // makes an OBTAIN_PUBLICATION action which is illegal
    action testObtainPub;
    testObtainPub.actionCode = OBTAIN_PUBLICATION;
    assert(isLegalAction(g,testObtainPub) == FALSE);

    // makes an OBTAIN_IP_PATENT action which is illegal
    action testObtainIPPat;
    testObtainIPPat.actionCode = OBTAIN_IP_PATENT;
    assert(isLegalAction(g,testObtainIPPat) == FALSE);

    /*
    // makes a move into the ocean that is illegal
    action testTheWaters;
    testTheWaters.destination = {R,L,0};
    assert(isLegalAction(g,makeAction(g, testTheWaters)) == FALSE);
    */

    // if it all passes, congratulate 'em!
    printf("All 'isLegalAction' function tests passed!\n");
    printf("You are awesome!");

    disposeGame(g);
}

void testgetKPIpoints(void) {
    // state what this does
    printf("Testing the 'getKPIpoints' function");

    // sets up a new game first of all
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    // check that the 'getKPIpoints' function gives 0 for each players'
    // KPI at the start of a game
    assert(getKPIpoints(g, UNI_A) == 0);
    assert(getKPIpoints(g, UNI_B) == 0);
    assert(getKPIpoints(g, UNI_C) == 0);

    // if it all passes, congratulate 'em!
    printf("All 'getKPIpoints' function tests passed!\n");
    printf("You are awesome!");

    disposeGame(g);
}


// the 'getARCs' function returns the number of ARC grants the
// specified player currently has
// this tests that function
void testgetARCs(void) {
    // state what this does
    printf("Testing the 'getARCs' function");

    // sets up a new game first of all
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    // check that the 'getARCs' function gives 0 for each players' number
    // of ARC grants at the start of a game
    assert(getARCs(g, UNI_A) == 0);
    assert(getARCs(g, UNI_B) == 0);
    assert(getARCs(g, UNI_C) == 0);

    // if it all passes, congratulate 'em!
    printf("All 'getARCs' function tests passed!\n");
    printf("You are awesome!");

    disposeGame(g);
}

void testgetGO8s(void) {
    // state what this does
    printf("Testing the 'getARCs' function");

    // sets up a new game first of all
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    // check that the 'getGO8s' function gives 0 for each players'
    // number of GO8 campuses at the start of a game
    assert(getGO8s(g, UNI_A) == 0);
    assert(getGO8s(g, UNI_B) == 0);
    assert(getGO8s(g, UNI_C) == 0);

    // if it all passes, congratulate 'em'!
    printf("All 'getGO8s' function tests passed!\n");
    printf("You are awesome!");

    disposeGame(g);
}


void testgetCampuses (void) {
    // state what this does
    printf("Testing the 'getCampuses' function");

    // sets up a new game first of all
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    // check that the 'getCampuses' function gives 2 for each players'
    // number of Campuses at the start of a game
    assert(getCampuses(g, UNI_A) == 2);
    assert(getCampuses(g, UNI_B) == 2);
    assert(getCampuses(g, UNI_C) == 2);

    // if it all passes, congratulate 'em'!
    printf("All 'getCampuses' function tests passed!\n");
    printf("You are awesome!");

    disposeGame(g);
}

void testgetIPs (void) {
    // state what this does
    printf("Testing the 'getIPs' function");

    // sets up a new game first of all
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    // check that the '' function gives 0 for each players'
    // number of IPs at the start of a game
    assert(getIPs(g, UNI_A) == 0);
    assert(getIPs(g, UNI_B) == 0);
    assert(getIPs(g, UNI_C) == 0);

    // if it all passes, congratulate 'em'!
    printf("All 'getIPs' function tests passed!\n");
    printf("You are awesome!");

    disposeGame(g);
}

void testgetPublications(void) {
    // state what this does
    printf("Testing the 'getPublications' function");

    // sets up a new game first of all
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    // check that the 'getStudents' function gives 0 for each players'
    // number of Publications at the start of a game
    assert(getPublications(g, UNI_A) == 0);
    assert(getPublications(g, UNI_B) == 0);
    assert(getPublications(g, UNI_C) == 0);

    // if it all passes, congratulate 'em'!
    printf("All 'getPublications' function tests passed!\n");
    printf("You are awesome!");

    disposeGame(g);
}


void testgetStudents (void) {
    // state what this does
    printf("Testing the 'getStudents' function");

    // sets up a new game first of all
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    // check that the 'getStudents' function gives 0 for each players'
    // number of ThD Students at the start of a game
    assert(getStudents(g, UNI_A,STUDENT_THD) == 0);
    assert(getStudents(g, UNI_B,STUDENT_THD) == 0);
    assert(getStudents(g, UNI_C,STUDENT_THD) == 0);

    // if it all passes, congratulate 'em'!
    printf("All 'getStudents' function tests passed!\n");
    printf("You are awesome!");

    disposeGame(g);
}

void testgetExchangeRate(void) {
    printf("Testing the 'getExchangeRate' function");

    // sets up a new game first of all
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game g = newGame (disciplines, dice);

    // check that the 'getExchangeRate' function gives 3 for each
    // player for each student type at the start of the game
    assert(getExchangeRate(g,UNI_A,STUDENT_BPS,STUDENT_THD) == 3);
    assert(getExchangeRate(g,UNI_B,STUDENT_BPS,STUDENT_THD) == 3);
    assert(getExchangeRate(g,UNI_C,STUDENT_THD,STUDENT_THD) == 3);

    // if it all passes, congratulate 'em'!
    printf("All 'getExchangeRate' function tests passed!\n");
    printf("You are awesome!");

    disposeGame(g);
}
