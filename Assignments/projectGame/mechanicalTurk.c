/*
 *  Mr Pass. Brain the size of a planet!
 *
 *  Proudly created by Richard Buckland
 *  Share Freely Creative Commons SA-BY-NC 3.0.
 *
 *  This implementation was made by Justin Ng
 *  for the pair of Justin Ng and Jonathan Wong
 *  Tutor class is wed13-oboe
 *  Tutor is Steven Strijakov
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "Game.h"
#include "mechanicalTurk.h"

#define MAX_LENGTH 150

action decideAction (Game g) {
    int player = getWhoseTurn(g);
    int bps = getStudents(g, player, STUDENT_BPS);
    int bqn = getStudents(g, player, STUDENT_BQN);
    int mj = getStudents(g, player, STUDENT_MJ);
    int mtv = getStudents(g, player, STUDENT_MTV);
    int mmy = getStudents(g, player, STUDENT_MMONEY);
    printf("I am player %d\n", getWhoseTurn(g));
    printf("I now have %d BPS students, %d BQN students, %d MJ students, %d MTV students and %d MMY students\n",bps, bqn, mj, mtv, mmy);

    action nextAction;
    nextAction.actionCode = PASS;

    action spinoff;
    spinoff.actionCode = START_SPINOFF;
    action getARC;
    getARC.actionCode = OBTAIN_ARC;
    action getCampus;
    getCampus.actionCode = BUILD_CAMPUS;
    action retrain;
    retrain.actionCode = RETRAIN_STUDENTS;

    if(nextAction.actionCode == PASS) {
        if(mtv < 3) {
            retrain.disciplineTo = STUDENT_MTV;
            if(mj > 3){
                retrain.disciplineFrom = STUDENT_MJ;
            } else if(mmy > 3){
                retrain.disciplineFrom = STUDENT_MMONEY;
            } else if(bps > 3){
                retrain.disciplineFrom = STUDENT_BPS;
            } else if(bqn > 3){
                retrain.disciplineFrom = STUDENT_BQN;
            } else {
                retrain.actionCode = PASS;
            }
        } else if(bps < 3) {
            retrain.disciplineTo = STUDENT_BPS;
            if(mj > 3){
                retrain.disciplineFrom = STUDENT_MJ;
            } else if(mtv > 3){
                retrain.disciplineFrom = STUDENT_MTV;
            } else if(mmy > 3){
                retrain.disciplineFrom = STUDENT_MMONEY;
            } else if(bqn > 3){
                retrain.disciplineFrom = STUDENT_BQN;
            } else {
                retrain.actionCode = PASS;
            }
        } else if(bqn < 3) {
            retrain.disciplineTo = STUDENT_BQN;
            if(mj > 3){
                retrain.disciplineFrom = STUDENT_MJ;
            } else if(mtv > 3){
                retrain.disciplineFrom = STUDENT_MTV;
            } else if(mmy > 3){
                retrain.disciplineFrom = STUDENT_MMONEY;
            } else if(bps > 3){
                retrain.disciplineFrom = STUDENT_BPS;
            } else {
                retrain.actionCode = PASS;
            }
        } else if(mj < 3) {
            retrain.disciplineTo = STUDENT_MJ;
            if(mtv > 3){
                retrain.disciplineFrom = STUDENT_MTV;
            } else if(mmy > 3){
                retrain.disciplineFrom = STUDENT_MMONEY;
            } else if(bps > 3){
                retrain.disciplineFrom = STUDENT_BPS;
            } else if(bqn > 3){
                retrain.disciplineFrom = STUDENT_BQN;
            } else {
                retrain.actionCode = PASS;
            }
        }
        nextAction = retrain;
    }
    printf("After attempting retraining, action code: %d", nextAction.actionCode);

    if(nextAction.actionCode == PASS) {
        if (bqn >= 1 && bps >= 1 && mj >= 1 && mtv >= 1) {
            char possiblePaths[73][MAX_LENGTH] = {"R","L","RL","RR","RLR","RLL","RRL","RRLL","RRLR","RRLRL","RRLRLL","RRLRLLR","RRLRLLRL","RRLRLLRLR","RRLRLLRLRL","RRLRLLRLRLL","RRLRLLL","RRLRLLRLL","RLRR","RLRRL","RLRRLL","RLRRLLL","RLRRLLLL","RLRRLLLLL","RLRRLLR","RLRRLLRL","RLRRLLRL","RLRRLLRLR","RLRRLLRLRL","RLRRLLRLRLL","RLRRLLRLL","RLRRLLRLLL","RLRRLLRLLLL","RLRRLLRLLR","RLRRLLRLLRR","RLRRLLRLLRRL","RLRRLLRLLRRLL","RLRRLLRLLRL","RLRRLLRLLRLR","RLRRLLRLLRLRR","LR","LRR","LRRL","LRRLR","LRRLRR","LRRLRL","LRRLRLR","LRRLRLRR","LRRLRLRL","LRRLRLRLR","LRL","LRLR","LRLRR","LRLRRR","LRLRRL","LRLRRLR","LRLRRLRR","LRLRRLRL","LRLRRLRLR","LRLRRLRLRR","LRLRRLRLRL","LRLRRLRLRLR","LRLRRLRLRLRR","LRLRL","LRLRLR","LRLRLRR","LRLRLRRR","LRLRLRL","LRLRLRLR","LRLRLRLRR","LRLRLRLRL","LRLRLRLRLR","LRLRLRLRLRR"};
            printf("Trying to find a valid campus to build...\n");
            int n = 0;
            strcpy(getCampus.destination, possiblePaths[n]);
            printf("Our string: %s\n", possiblePaths[n]);
            while(isLegalAction(g, getARC) != TRUE) {
                printf("Path not legal: %s\n", getCampus.destination);
                strcpy(getCampus.destination, possiblePaths[n]);
                n++;
                if (n > 72) {
                    getCampus.actionCode = PASS;
                break;
                }
            }
        nextAction = getCampus;
        }
    }
    printf("After attempting a campus, action code: %d", nextAction.actionCode);

    if(nextAction.actionCode == PASS) {
        if(bqn >= 1 && bps >= 1) {
            char possiblePaths[73][MAX_LENGTH] = {"R","L","RL","RR","RLR","RLL","RRL","RRLL","RRLR","RRLRL","RRLRLL","RRLRLLR","RRLRLLRL","RRLRLLRLR","RRLRLLRLRL","RRLRLLRLRLL","RRLRLLL","RRLRLLRLL","RLRR","RLRRL","RLRRLL","RLRRLLL","RLRRLLLL","RLRRLLLLL","RLRRLLR","RLRRLLRL","RLRRLLRL","RLRRLLRLR","RLRRLLRLRL","RLRRLLRLRLL","RLRRLLRLL","RLRRLLRLLL","RLRRLLRLLLL","RLRRLLRLLR","RLRRLLRLLRR","RLRRLLRLLRRL","RLRRLLRLLRRLL","RLRRLLRLLRL","RLRRLLRLLRLR","RLRRLLRLLRLRR","LR","LRR","LRRL","LRRLR","LRRLRR","LRRLRL","LRRLRLR","LRRLRLRR","LRRLRLRL","LRRLRLRLR","LRL","LRLR","LRLRR","LRLRRR","LRLRRL","LRLRRLR","LRLRRLRR","LRLRRLRL","LRLRRLRLR","LRLRRLRLRR","LRLRRLRLRL","LRLRRLRLRLR","LRLRRLRLRLRR","LRLRL","LRLRLR","LRLRLRR","LRLRLRRR","LRLRLRL","LRLRLRLR","LRLRLRLRR","LRLRLRLRL","LRLRLRLRLR","LRLRLRLRLRR"};
            printf("trying to find a valid arc to build\n");
            int n = 0;
            strcpy(getARC.destination, possiblePaths[n]);
            printf("our string:%s\n", possiblePaths[n]);
            while(isLegalAction(g, getARC) != TRUE) {
                printf("path not legal: %s\n", getARC.destination);
                strcpy(getARC.destination, possiblePaths[n]);
                n++;
                if(n > 72) {
                    getARC.actionCode = PASS;
                    break;
                }
            }
            nextAction = getARC;
        }
    }
    printf("After attempting an ARC, action code: %d", nextAction.actionCode);

    if(nextAction.actionCode == PASS) {
        if(isLegalAction(g, spinoff) == TRUE) {
            nextAction = spinoff;
        }
    }
    printf("After attempting a spinoff, action code: %d", nextAction.actionCode);

    return nextAction;
}
