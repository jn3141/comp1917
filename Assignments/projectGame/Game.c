/*
 *  Justin Ng, Jeff Zhang, Shashika Rupasinghe, Jonathon Wong
 *  Tutor class is wed13-oboe
 *  Tutor is Steven Strijakov
 *  File first created on the 04/05/2016
 *  This program acts as the basis for the game.
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include "Game.h"

#define NUM_DISCIPLINES 6
#define TILE_COLUMNS 5
#define TILE_ROWS 5
#define ARC_COLUMNS 11
#define ARC_ROWS 10
#define CAMPUS_COLUMNS 6
#define CAMPUS_ROWS 11
#define START_CAMPUSES 2
#define TERRA_NULLIS -1
#define INITIAL_KPI 0
#define INITIAL_CAMPUSES 2

#define CAMPUS_KPI_VALUE 10
#define GO8_KPI_VALUE 10
#define ARC_KPI_VALUE 2
#define PRESTIGE_KPI_VALUE 10
#define IP_KPI_VALUE 10

#define IS_ARC 0
#define IS_CAMPUS 1

#define START_CAMPUS {2,0}
#define START_DIRECTION 0

#define CAMPUS_REQ 1
#define SPINOFF_REQ 1
#define ARC_REQ 1
#define GO8_MJ 2
#define GO8_MMONEY 3

typedef struct _tile {
    int discipline;
    int diceValue;
} Tile;

typedef struct _campus {
    int ownedBy;
    int go8Status;
} Campus;

typedef struct _grid {
    int column;
    int row;
} Grid;

typedef struct _uniStats {
    char *name;
    int numKPI;
    int numPubs;
    int numARCs;
    int numIPs;
    int numGO8s;
    int numCampuses;
    /*
     * for numStudents, the item number of the array corresponds to the
     * number they have of that student discipline
     * e.g. numStudents[0] = numStudents[STUDENT_THD] = number of ThD
     * students
     */
    int numStudents[NUM_DISCIPLINES];
    /*
     * for convCentre, the item number of the array corresponds to
     * whether or not the conversion centre for that discipline is
     * owned by them
     * e.g. if they own the conversion centre for the ThD discipline
     * then convCentre[STUDENT_THD] = TRUE
     */
    int convCentre[NUM_DISCIPLINES];
} UniStats;

typedef struct _game {
    /*
     * like noted in Game.h, tiles are numbered by column, then row,
     * e.g. the top starting tile is [2][0], the one to the left of that
     * is [1][0] and the tile below the starting tile is [2][1]
     */
    Tile gameTiles[NUM_REGIONS];

    /*
     * similarly, the ARCs on the board are numbered by column, then
     * by row, except with a 2D-array; the value held in each element
     * of this 2D-array is the name of the owner
     * e.g. the top starting horizontal ARC is [5][0], the one to the
     * left of that is [4][0]
     */
    int gameARCs[ARC_COLUMNS][ARC_ROWS];

    /*
     * similarly, the campuses on the board are numbered by column, then
     * by row
     * e.g. the top starting initial campus is, the one to the
     * right of that is [4][0]
     */
    Campus gameCampuses[CAMPUS_COLUMNS][CAMPUS_ROWS];

    // this is an array of the Unis competing in this game
    UniStats gameUnis[NUM_UNIS];

    // stores the current turn
    int currentTurn;

    // stores the value of a dice roll
    int diceScore;

    // stores the number of GO8 campuses on the board
    int totalGO8;
} game;

static int isLegalGrid (Grid coordinate, int type);
static Grid pathToVertexGrid (path pathToVertex);
static Grid pathToARCGrid (path pathToARC);

Game newGame(int discipline[], int dice[]) {
    Game g = malloc(sizeof(game));

    // setting up the basics...
    g->currentTurn = TERRA_NULLIS;
    g->diceScore = 0;

    // tiling the board...
    int tiler = 0;
    while(tiler < NUM_REGIONS) {
        g->gameTiles[tiler].discipline = discipline[tiler];
        g->gameTiles[tiler].diceValue = dice[tiler];
        tiler++;
    }

    // emptying the campuses...
    int column = 0;
    int row = 0;
    while(column < CAMPUS_COLUMNS) {
        while(row < CAMPUS_ROWS) {
            g->gameCampuses[column][row].ownedBy = NO_ONE;
            g->gameCampuses[column][row].go8Status = FALSE;
            row++;
        }
        column++;
    }

    // emptying the arcs...
    column = 0;
    row = 0;
    while(column < ARC_COLUMNS) {
        while(row < ARC_ROWS) {
            g->gameARCs[column][row] = NO_ONE;
            row++;
        }
        column++;
    }

    // the unis were founded...
    g->gameCampuses[2][0].ownedBy = UNI_A;
    g->gameCampuses[3][10].ownedBy = UNI_A;
    g->gameCampuses[0][1].ownedBy = UNI_B;
    g->gameCampuses[5][5].ownedBy = UNI_B;
    g->gameCampuses[0][6].ownedBy = UNI_C;
    g->gameCampuses[5][0].ownedBy = UNI_C;


    // they were given prestigious names...
    g->gameUnis[0].name = "UNI_A";
    g->gameUnis[1].name = "UNI_B";
    g->gameUnis[2].name = "UNI_C";

    // and bestowed with stats...
    int statSetter = 1;
    while(statSetter <= NUM_UNIS) {
        g->gameUnis[statSetter].numKPI = INITIAL_KPI;
        g->gameUnis[statSetter].numPubs = 0;
        g->gameUnis[statSetter].numARCs = 0;
        g->gameUnis[statSetter].numIPs = 0;
        g->gameUnis[statSetter].numGO8s = 0;
        g->gameUnis[statSetter].numCampuses = INITIAL_CAMPUSES;
        g->gameUnis[statSetter].numStudents[STUDENT_THD] = 0;
        g->gameUnis[statSetter].numStudents[STUDENT_BPS] = 3;
        g->gameUnis[statSetter].numStudents[STUDENT_BQN] = 3;
        g->gameUnis[statSetter].numStudents[STUDENT_MJ] = 1;
        g->gameUnis[statSetter].numStudents[STUDENT_MTV] = 1;
        g->gameUnis[statSetter].numStudents[STUDENT_MMONEY] = 1;
        g->gameUnis[statSetter].convCentre[STUDENT_THD] = FALSE;
        g->gameUnis[statSetter].convCentre[STUDENT_BPS] = FALSE;
        g->gameUnis[statSetter].convCentre[STUDENT_BQN] = FALSE;
        g->gameUnis[statSetter].convCentre[STUDENT_MJ] = FALSE;
        g->gameUnis[statSetter].convCentre[STUDENT_MTV] = FALSE;
        g->gameUnis[statSetter].convCentre[STUDENT_MMONEY] = FALSE;
        statSetter++;
    }
    return g;
}

void disposeGame(Game g) {
    free(g);
    g = NULL;
    assert(g == NULL);
    return;
}

void makeAction(Game g, action a) {
    assert(isLegalAction(g, a) == TRUE);
    int currentUni = getWhoseTurn(g);
    Grid campus = pathToVertexGrid(a.destination);
    Grid ARC = pathToARCGrid(a.destination);


    if (a.actionCode == PASS) {

    } else if (a.actionCode == BUILD_CAMPUS) {
        // Deduct students
        g->gameUnis[currentUni].numStudents[STUDENT_BQN]--;
        g->gameUnis[currentUni].numStudents[STUDENT_BPS]--;
        g->gameUnis[currentUni].numStudents[STUDENT_MJ]--;
        g->gameUnis[currentUni].numStudents[STUDENT_MTV]--;

        // Change game state
        g->gameCampuses[campus.column][campus.row].ownedBy = currentUni;
        g->gameUnis[currentUni].numKPI += CAMPUS_KPI_VALUE;
        g->gameUnis[currentUni].numCampuses++;
    } else if (a.actionCode == BUILD_GO8) {
        // Deduct students
        g->gameUnis[currentUni].numStudents[STUDENT_MJ] -= 2;
        g->gameUnis[currentUni].numStudents[STUDENT_MMONEY] -= 2;

        // Change game state
        g->gameCampuses[campus.column][campus.row].go8Status = TRUE;
        g->totalGO8++;
        g->gameUnis[currentUni].numKPI += GO8_KPI_VALUE;
        g->gameUnis[currentUni].numGO8s++;
    } else if (a.actionCode == OBTAIN_ARC) {
        // Deduct students
        g->gameUnis[currentUni].numStudents[STUDENT_BQN]--;
        g->gameUnis[currentUni].numStudents[STUDENT_BPS]--;

        // Change game state
        g->gameARCs[ARC.column][ARC.row] = currentUni;
        g->gameUnis[currentUni].numKPI += ARC_KPI_VALUE;
        g->gameUnis[currentUni].numARCs++;
        if (getMostARCs(g) == currentUni) {
            g->gameUnis[currentUni].numARCs--;
            int previousMostARCs = getMostARCs(g);
            g->gameUnis[currentUni].numARCs++;
            if(currentUni == previousMostARCs) {

            } else if(previousMostARCs == NO_ONE) {
                g->gameUnis[currentUni].numKPI += PRESTIGE_KPI_VALUE;
            } else if(previousMostARCs == UNI_A) {
                g->gameUnis[currentUni].numKPI += PRESTIGE_KPI_VALUE;
                g->gameUnis[UNI_A].numKPI -= PRESTIGE_KPI_VALUE;
            } else if(previousMostARCs == UNI_B) {
                g->gameUnis[currentUni].numKPI += PRESTIGE_KPI_VALUE;
                g->gameUnis[UNI_B].numKPI -= PRESTIGE_KPI_VALUE;
            } else if(previousMostARCs == UNI_C) {
                g->gameUnis[currentUni].numKPI += PRESTIGE_KPI_VALUE;
                g->gameUnis[UNI_C].numKPI -= PRESTIGE_KPI_VALUE;
            }
        }
    } else if (a.actionCode == START_SPINOFF) {
        // Deduct students
        g->gameUnis[currentUni].numStudents[STUDENT_MJ]--;
        g->gameUnis[currentUni].numStudents[STUDENT_MTV]--;
        g->gameUnis[currentUni].numStudents[STUDENT_MMONEY]--;

        int random = ((rand() % 3) + 1);
        if (random == 1) {
            a.actionCode = OBTAIN_PUBLICATION;
        } else {
            a.actionCode = OBTAIN_IP_PATENT;
        }
        if (a.actionCode == OBTAIN_PUBLICATION) {
            // Change game state
            g->gameUnis[currentUni].numPubs++;
            if (getMostPublications(g) == currentUni) {
                g->gameUnis[currentUni].numPubs--;
                int previousMostPubs = getMostPublications(g);
                g->gameUnis[currentUni].numPubs++;
                if(currentUni == previousMostPubs) {

                } else if(previousMostPubs == NO_ONE) {
                    g->gameUnis[currentUni].numKPI += PRESTIGE_KPI_VALUE;
                } else if(previousMostPubs == UNI_A) {
                    g->gameUnis[currentUni].numKPI += PRESTIGE_KPI_VALUE;
                    g->gameUnis[UNI_A].numKPI -= PRESTIGE_KPI_VALUE;
                } else if(previousMostPubs == UNI_B) {
                    g->gameUnis[currentUni].numKPI += PRESTIGE_KPI_VALUE;
                    g->gameUnis[UNI_B].numKPI -= PRESTIGE_KPI_VALUE;
                } else if(previousMostPubs == UNI_C) {
                    g->gameUnis[currentUni].numKPI += PRESTIGE_KPI_VALUE;
                    g->gameUnis[UNI_C].numKPI -= PRESTIGE_KPI_VALUE;
                }
            }
        } else if (a.actionCode == OBTAIN_IP_PATENT) {
            // Change game state
            g->gameUnis[currentUni].numKPI += IP_KPI_VALUE;
            g->gameUnis[currentUni].numIPs++;
        }
    } else if (a.actionCode == RETRAIN_STUDENTS) {
        int exchangeRate = getExchangeRate(g,currentUni,a.disciplineFrom,a.disciplineTo);
        g->gameUnis[currentUni].numStudents[a.disciplineFrom] -= exchangeRate;
        g->gameUnis[currentUni].numStudents[a.disciplineTo]++;
    }
    return;
}

void throwDice(Game g, int diceScore) {
    assert(g->currentTurn >= TERRA_NULLIS);
    assert(diceScore >=2 && diceScore <= 12);
    g->currentTurn++;
    return;
}

int getDiscipline(Game g, int regionID) {
    int discipline = g->gameTiles[regionID].discipline;
    return discipline;
}

int getDiceValue(Game g, int regionID) {
    int diceValue = g->gameTiles[regionID].diceValue;
    return diceValue;
}

int getMostARCs(Game g) {
    int MostARCs = 0;

	if(getARCs(g, UNI_A) > getARCs(g, UNI_B) &&
        getARCs(g, UNI_A) > getARCs(g, UNI_C)) {
		MostARCs = UNI_A;
	} else if(getARCs(g, UNI_B) > getARCs(g, UNI_A) &&
        getARCs(g, UNI_B) > getARCs(g, UNI_C)) {
		MostARCs = UNI_B;
	} else if(getARCs(g, UNI_C) > getARCs(g, UNI_A) &&
        getARCs(g, UNI_C) > getARCs(g, UNI_B)) {
		MostARCs = UNI_C;
	} else {
        MostARCs = NO_ONE;
    }

	return MostARCs;
}

int getMostPublications(Game g) {
    int MostPubs = 0;

	if(getPublications(g, UNI_A) > getPublications(g, UNI_B) &&
        getPublications(g, UNI_A) > getPublications(g, UNI_C)) {
		MostPubs = UNI_A;
	} else if(getPublications(g, UNI_B) > getPublications(g, UNI_A) &&
        getPublications(g, UNI_B) > getPublications(g, UNI_C)) {
		MostPubs = UNI_B;
	} else if(getPublications(g, UNI_C) > getPublications(g, UNI_A) &&
        getPublications(g, UNI_C) > getPublications(g, UNI_B)) {
		MostPubs = UNI_C;
	}

	return MostPubs;
}

int getTurnNumber(Game g) {
    int turnNumber = g->currentTurn;
    return turnNumber;
}

int getWhoseTurn(Game g) {
    int whoseTurn = g->currentTurn % 3 + 1;
    return whoseTurn;
}

int getCampus(Game g, path pathToVertex) {
    Grid campusGrid = pathToVertexGrid(pathToVertex);
    int campusCode = g->gameCampuses[campusGrid.column][campusGrid.row].ownedBy;
    return campusCode;
}

int getARC(Game g, path pathToEdge) {
    Grid ARCGrid = pathToARCGrid(pathToEdge);
    int ARC = g->gameARCs[ARCGrid.column][ARCGrid.row];
    return ARC;
}

int isLegalAction (Game g, action a) {
    int isLegal = FALSE;
	int currentUni = getWhoseTurn(g);
    int numBPS = g->gameUnis[currentUni].numStudents[STUDENT_BPS];
    int numBQN = g->gameUnis[currentUni].numStudents[STUDENT_BQN];
    int numMJ = g->gameUnis[currentUni].numStudents[STUDENT_MJ];
    int numMTV = g->gameUnis[currentUni].numStudents[STUDENT_MTV];
    int numMMONEY = g->gameUnis[currentUni].numStudents[STUDENT_MMONEY];

    int pathLength = strlen(a.destination);
    char adjacentR[pathLength + 1];
    char adjacentL[pathLength + 1];
    char adjacentB[pathLength + 1];
    char adjacentBL[pathLength + 2];
    char adjacentBR[pathLength + 2];

    strcpy(adjacentR,a.destination);
    strcpy(adjacentL,a.destination);
    strcpy(adjacentB,a.destination);
    strcpy(adjacentBL,a.destination);
    strcpy(adjacentBR,a.destination);

    adjacentR[pathLength] = 'R';
    adjacentL[pathLength] = 'L';
    adjacentB[pathLength] = 'B';
    adjacentBL[pathLength] = 'B';
    adjacentBL[pathLength + 1] = 'L';
    adjacentBR[pathLength] = 'B';
    adjacentBR[pathLength + 1] = 'R';

	if(g->currentTurn == TERRA_NULLIS) {
		isLegal = FALSE;
	} else {
		if(a.actionCode == PASS) {
			isLegal = TRUE;
		} else if(a.actionCode == BUILD_CAMPUS) {
			if(isLegalGrid(pathToVertexGrid(a.destination), IS_CAMPUS) == TRUE) {
				if(numBPS >= CAMPUS_REQ && numBQN >= CAMPUS_REQ && numMJ >= CAMPUS_REQ && numMTV >= CAMPUS_REQ) {
					if(getARC(g,adjacentR) == currentUni || getARC(g,adjacentL) == currentUni || getARC(g,adjacentB) == currentUni) {
                        if(getCampus(g,adjacentR) == NO_ONE && getCampus(g,adjacentL) == NO_ONE && getCampus(g,adjacentB) == NO_ONE) {
                            if(getCampus(g,a.destination) == NO_ONE) {
                                isLegal = TRUE;
                            }
                        }
					} else {
						isLegal = FALSE;
					}
				} else {
					isLegal = FALSE;
				}
			} else {
				isLegal = FALSE;
			}
		} else if(a.actionCode == BUILD_GO8) {
			if(isLegalGrid(pathToVertexGrid(a.destination), IS_CAMPUS) == TRUE) {
				if(getCampus(g, a.destination) == currentUni && numMJ >= GO8_MJ && numMMONEY >= GO8_MMONEY) {
				    isLegal = TRUE;
				} else {
				    isLegal = FALSE;
				}
			} else {
				isLegal = FALSE;
			}
		} else if(a.actionCode == OBTAIN_ARC) {
            if(isLegalGrid(pathToARCGrid(a.destination), IS_ARC) == TRUE) {
				if(numBQN >= ARC_REQ && numBPS >= ARC_REQ) {
					if(getARC(g,a.destination) == NO_ONE) {
                        if(getARC(g,adjacentR) == currentUni || getARC(g,adjacentL) == currentUni || getARC(g,adjacentB) == currentUni || getARC(g,adjacentBL) == currentUni || getARC(g,adjacentBR) == currentUni) {
                            isLegal = TRUE;
                        } else if(getCampus(g,a.destination) == currentUni || getCampus(g,adjacentB) == currentUni) {
                            isLegal = TRUE;
                        }
					} else {
						isLegal = FALSE;
					}
				} else {
					isLegal = FALSE;
				}
			} else {
				isLegal = FALSE;
			}
		} else if(a.actionCode == START_SPINOFF) {
			if(numMMONEY >= SPINOFF_REQ && numMJ >= SPINOFF_REQ && numMTV >= SPINOFF_REQ) {
				isLegal = TRUE;
			} else {
				isLegal = FALSE;
			}
		} else if(a.actionCode == OBTAIN_IP_PATENT) {
			isLegal = FALSE;
		} else if(a.actionCode == OBTAIN_PUBLICATION) {
			isLegal = FALSE;
		} else if(a.actionCode == RETRAIN_STUDENTS) {
			if(g->gameUnis[currentUni].numStudents[a.disciplineFrom] >= getExchangeRate(g, currentUni, a.disciplineFrom, a.disciplineTo)) {
				isLegal = TRUE;
			} else {
				isLegal = FALSE;
			}
		} else {
			isLegal = FALSE;
		}
	}
	return isLegal;
}

int getKPIpoints (Game g, int player) {
    int KPIpoints = g->gameUnis[player].numKPI;
    return KPIpoints;
}

int getARCs (Game g, int player) {
    int ARCs = g->gameUnis[player].numARCs;
    return ARCs;
}

int getGO8s (Game g, int player) {
    int GO8s = g->gameUnis[player].numGO8s;
    return GO8s;
}

int getCampuses (Game g, int player) {
    int campuses = g->gameUnis[player].numCampuses;
    return campuses;
}

int getIPs (Game g, int player) {
    int IPs = g->gameUnis[player].numIPs;
    return IPs;
}

int getPublications (Game g, int player) {
    int publications = g->gameUnis[player].numPubs;
    return publications;
}

int getStudents (Game g, int player, int discipline) {
    int studentCount = g->gameUnis[player].numStudents[discipline];
    return studentCount;
}

int getExchangeRate (Game g, int player, int disciplineFrom, int disciplineTo) {
    int exchangeRate = 3;

    if (disciplineFrom == 0) {
        exchangeRate = 0;
    } else if (g->gameUnis[player].convCentre[disciplineTo] == TRUE) {
        exchangeRate = 2;
    } else {
        exchangeRate = 3;
    }

    return exchangeRate;
}

static int isLegalGrid (Grid coordinate, int type) {

    int isLegalGrid = FALSE;

    // Checking whether an ARC grid value is legal
	if(type == IS_ARC) {
		if(coordinate.column < 0 || coordinate.row < 0 || coordinate.column > 10 || coordinate.row > 10 ) {
			isLegalGrid = FALSE;
		} else if ((coordinate.column == 0 || coordinate.column == 10) && (coordinate.row > 6)) {
			isLegalGrid = FALSE;
		} else if ((coordinate.column == 1 || coordinate.column == 9) && (coordinate.row > 4)) {
			isLegalGrid = FALSE;
		} else if ((coordinate.column == 2 || coordinate.column == 8) && (coordinate.row > 8)) {
			isLegalGrid = FALSE;
		} else if ((coordinate.column == 3 || coordinate.column == 7) && (coordinate.row > 5)) {
			isLegalGrid = FALSE;
		} else if ((coordinate.column == 4 || coordinate.column == 6) && (coordinate.row > 10)) {
			isLegalGrid = FALSE;
		} else if ((coordinate.column == 5) && (coordinate.row > 6)){
			isLegalGrid = FALSE;
		} else {
            isLegalGrid = TRUE;
        }
	}
	// Checking whether an ARC grid value is legal
	if(type == IS_CAMPUS) {
		if(coordinate.column < 0 || coordinate.row < 0){
			isLegalGrid = FALSE;
		} else if ((coordinate.column == 0 || coordinate.column == 5) && (coordinate.row > 6)) {
			isLegalGrid = FALSE;
		} else if ((coordinate.column == 1 || coordinate.column == 4) && (coordinate.row > 8)) {
			isLegalGrid = FALSE;
		} else if ((coordinate.column == 2 || coordinate.column == 3) && (coordinate.row > 10)) {
			isLegalGrid = FALSE;
		} else {
            isLegalGrid = TRUE;
        }
	}
	assert(isLegalGrid == TRUE);
	return isLegalGrid;
}

static Grid pathToVertexGrid (path pathToVertex) {
    Grid location = START_CAMPUS;
    /*
     * directions are clockwise, with 0 being pointing towards the
     * the centre of the tile at START_CAMPUS, and 5 pointing
     * towards the right
     */
    int direction = START_DIRECTION;
	int i = 0;
	int validDirection = FALSE;


	while(pathToVertex[i] != 0){
		if (pathToVertex[i] == 'R') {
			direction = (direction + 1) % 6;
            validDirection = TRUE;
		} else if(pathToVertex[i] == 'L') {
			direction = (direction + 5) % 6;
            validDirection = TRUE;
		} else if(pathToVertex[i] == 'B') {
			direction = (direction + 3) % 6;
            validDirection = TRUE;
		} else {
			validDirection = FALSE;
		}

        assert(validDirection = TRUE);

		//if(validDirection = TRUE) {
		if(direction == 3 || direction == 4) {
			location.row--;
		} else if(direction == 0 || direction == 1) {
			location.row++;
		} else if(direction == 2) {
            location.row--;
            location.column--;
		} else if(direction == 5) {
            location.row++;
            location.column++;
		}
		i++;
	}
	return location;
}

static Grid pathToARCGrid (path pathToARC) {
    Grid frontLocation = START_CAMPUS;
    Grid backLocation = START_CAMPUS;
    Grid location = START_CAMPUS;
    /*
     * directions are clockwise, with 0 being pointing towards the
     * the centre of the tile at START_CAMPUS, and 5 pointing
     * towards the right
     */
    int direction = START_DIRECTION;
    int backDirection = START_DIRECTION;
	int i = 0;
	int validDirection = FALSE;


	while(pathToARC[i] != 0){
		if (pathToARC[i] == 'R') {
			direction = (direction + 1) % 6;
            validDirection = TRUE;
		} else if(pathToARC[i] == 'L') {
			direction = (direction + 5) % 6;
            validDirection = TRUE;
		} else if(pathToARC[i] == 'B') {
			direction = (direction + 3) % 6;
            validDirection = TRUE;
		} else {
			validDirection = FALSE;
		}

        assert(validDirection = TRUE);

		if(direction == 3 || direction == 4) {
			frontLocation.row--;
		} else if(direction == 0 || direction == 1) {
			frontLocation.row++;
		} else if(direction == 2) {
            frontLocation.row--;
            frontLocation.column--;
		} else if(direction == 5) {
            frontLocation.row++;
            frontLocation.column++;
		}

		i++;
	}

    backDirection = (direction + 3) % 6;
    backLocation = frontLocation;
    if(backDirection == 3 || backDirection == 4) {
        backLocation.row--;
    } else if(backDirection == 0 || backDirection == 1) {
        backLocation.row++;
    } else if(backDirection == 2) {
        backLocation.row--;
        backLocation.column--;
    } else if(backDirection == 5) {
        backLocation.row++;
        backLocation.column++;
    }

    if(direction == 2) {
        if(frontLocation.column <= 2) {
            location.row = frontLocation.row/2;
        } else if(frontLocation.column >= 3) {
            location.row = backLocation.row/2;
        }
    } else if(direction == 5) {
        if(frontLocation.column >= 3) {
            location.row = frontLocation.row/2;
        } else if(frontLocation.column <= 2) {
            location.row = backLocation.row/2;
        }
    } else if(direction == 3 || direction == 4) {
        location.row = frontLocation.row;
    } else if(direction == 0 || direction == 1) {
        location.row = backLocation.row;
    }

    location.column = frontLocation.column + backLocation.column;

	return location;
}
