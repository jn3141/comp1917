/*
 *  Mr Pass. Brain the size of a planet!
 *
 *  Proudly created by Richard Buckland
 *  Share Freely Creative Commons SA-BY-NC 3.0.
 *
 *  This implementation was made by Justin Ng
 *  for the pair of Justin Ng and Jonathan Wong
 *  Tutor class is wed13-oboe
 *  Tutor is Steven Strijakov
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "Game.h"
#include "mechanicalTurk.h"

action decideAction (Game g) {

    action nextAction;

    int uni = getWhoseTurn(g);
    // int numBPS = getStudents(g, player, STUDENT_BPS);
    // int numBQN = getStudents(g, player, STUDENT_BQN);
    int numMJ = getStudents(g, uni, STUDENT_MJ);
    int numMTV = getStudents(g, uni, STUDENT_MTV);
    int numMMY = getStudents(g, uni, STUDENT_MMONEY);

    if(numMJ >= 1 && numMTV >= 1 && numMMY >=1) {
        nextAction.actionCode = START_SPINOFF;
    } else {
        nextAction.actionCode = PASS;
    }

    return nextAction;
}
