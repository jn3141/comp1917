/*
 *  Justin Ng & Jeff Zhang
 *  Tutor class is wed13-oboe
 *  Tutor is Steven Strijaskov
 *  File first created on the 13/04/2016
 *  This program defines the functions used to colour our Mandelbrot plot.
 */

#include "pixelColor.h"

unsigned char stepsToRed (int steps) {
    unsigned char red;
    red = steps % 16;
    return red;
}

unsigned char stepsToBlue (int steps){
    unsigned char blue = 0;
    blue = steps % 32;
    return blue;
}

unsigned char stepsToGreen (int steps){
    unsigned char green;
    green = steps % 64;
    return green;
}