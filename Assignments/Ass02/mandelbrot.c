/*  
 *  Justin Ng & Jeff Zhang
 *  Tutor class is wed13-oboe
 *  Tutor is Steven Strijakov
 *  File first created on the 13/04/2016
 *  This program acts as a server that plots the Mandelbrot set
 *  and either presents it via a viewer, or as a BMP file.
 *  The set is done via 256 iterations of the equation.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <netinet/in.h>
#include <unistd.h>
#include <math.h>

#include "pixelColor.h"
#include "mandelbrot.h"

#define SIZE 512
#define HALFSIZE (SIZE/2)
#define MAXIMUM_ITERATIONS 256
#define LIMIT 4

#define BREAD_SERVER_VERSION 2.0
#define REQUEST_BUFFER_SIZE 1000
#define DEFAULT_PORT 1997
#define NUMBER_OF_PAGES_TO_SERVE 9001 // It's over 9000!

int escapeSteps (double x, double y);
int waitForConnection (int serverSocket);
int makeServerSocket (int portNumber);
void serveBread (int socket);
void serveBreadSlice (int socket, double x, double y, int zoom);

int main (int argc, char * argv[]) {
    double x = 0.0;
    double y = 0.0;
    int zoom = 0;

    int serverSocket = makeServerSocket(DEFAULT_PORT);
    char request[REQUEST_BUFFER_SIZE];
    int numberServed = 0;
    char requested[256];
    
    printf ("************************************\n");
    printf ("Starting Almondbread server %f\n", BREAD_SERVER_VERSION);
    printf ("Serving steaming hot almondbread since 2016\n");
    printf ("Access this server at http://localhost:%d/ if you're at uni\n", DEFAULT_PORT);
    printf ("Access this server at http://nameOfCSEServer.cse.unsw.edu.au:%d/ if you're at home\n", DEFAULT_PORT);
    printf ("************************************\n");
    
    while (numberServed < NUMBER_OF_PAGES_TO_SERVE) {
        printf ("*** So far served %d pages ***\n", numberServed);
 
        // Step 1) Wait for a request to be sent from a web browser, 
        // then open a new connection for this conversation.
        int connectionSocket = waitForConnection (serverSocket);
 
        // Step 2) Read the first line of the request.
        int bytesRead = recv (connectionSocket, request, sizeof(request) - 1, 0);
        
        // Step 3) Check that we were able to read some data from the connection.
        assert (bytesRead >= 0);
        
        // Step 4) Echo the entire request to the console for debugging.
        printf (" *** Received http request ***\n %s\n", request);
        
        // Step 5) Read in what the client requested.
        sscanf (request, "GET %s HTTP/", requested);
 
        // Step 6) Serve client what they requested: root directory, or specific BMP
        if (strcmp (requested, "/") == 0) {
            printf (" *** Sending bread ***\n");
            serveBread(connectionSocket);
        } else {
            printf (" *** Sending slice of bread ***\n");
            sscanf (requested, "/tile_x%lf_y%lf_z%d.bmp", &x, &y, &zoom);
            printf ("requested x-coordinate is %lf\n", x);
            printf ("requested y-coordinate is %lf\n", y);
            printf ("requested zoom is %d\n", zoom);
            serveBreadSlice(connectionSocket, x, y, zoom);
        }
        
        // Step 8) Close the connection after sending the page.
        close (connectionSocket);
        numberServed++;
        
        // Step 9) ????
        
        // Step 10) PROFIT!
    }

    return EXIT_SUCCESS;
}

int escapeSteps (double x, double y) {
    double real = 0;
    double newReal = 0;
    double im = 0;
    double newIm = 0;
    double mod = 0;
    int steps = 0; 
    while (steps < MAXIMUM_ITERATIONS && mod < LIMIT) {
        newReal = pow (real,2) - pow (im,2) + x;
        newIm = 2 * real * im + y;
        mod = pow (newReal,2) + pow (newIm,2);
        real = newReal;
        im = newIm;
        steps++;
    }
    return steps;
}

//Serve the HTML tileviewer to the web server.
void serveBread (int socket) {
    char* message;

    // Sends the http response.
    message =
        "HTTP/1.0 200 Found\n"
        "Content-Type: text/html\r\n"
        "\r\n"
        "<html>\n"
        "   <head><title>Bread Server</title></head>\n"
        "</html>\n"
        "<!DOCTYPE html>\n"
        "<script src=\"//almondbread.cse.unsw.edu.au/tiles.js\"></script>"
        "\n";
   printf ("about to send=> %s\n", message);
   write (socket, message, strlen (message));
}

//Serve the BMP to web server.
void serveBreadSlice (int socket, double x, double y, int zoom) {
    char* message;

    // Sends the http response header.
    message =
    "HTTP/1.0 200 OK\r\n"
    "Content-Type: image/bmp\r\n"
    "\r\n";
    printf ("about to send=> %s\n", message);
    write (socket, message, strlen (message));
   
    // Now send the BMP.

    // Writes out the BMP header.
    unsigned char bmp[] = {
        0x42,0x4d,0x5a,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x36,0x00,0x00,0x00,0x28,0x00,
        0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x02,
        0x00,0x00,0x01,0x00,0x18,0x00,0x00,0x00,
        0x00,0x00,0x24,0x00,0x00,0x00,0x13,0x0b,
        0x00,0x00,0x13,0x0b,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00};
    write (socket, bmp, sizeof(bmp));
    
    // Writes out the actual pixels of the BMP.
    double pixelDist = pow (2, -zoom);
    double xCurrent = x - pixelDist * (HALFSIZE);
    double yCurrent = y - pixelDist * (HALFSIZE);
    double xMax = x + pixelDist * (HALFSIZE);
    double yMax = y + pixelDist * (HALFSIZE);
    double xInitial = xCurrent;
    char RGB[3];

    while (yCurrent < yMax) {
        while (xCurrent < xMax) {
            RGB[0] = stepsToBlue (escapeSteps (xCurrent, yCurrent));
            RGB[1] = stepsToGreen (escapeSteps (xCurrent, yCurrent));
            RGB[2] = stepsToRed (escapeSteps (xCurrent, yCurrent));
            xCurrent = xCurrent + pixelDist;
            write (socket, RGB, sizeof(RGB));
        }
        yCurrent = yCurrent + pixelDist;
        xCurrent = xInitial;
   }
}

// Start the server listening on the specified port number.
int makeServerSocket (int portNumber) {
    // Create the socket
    int serverSocket = socket (AF_INET, SOCK_STREAM, 0);
    
    // Check there was no error in opening the socket.
    assert (serverSocket >= 0);

    // Bind the socket to the listening port.
    struct sockaddr_in serverAddress;
    serverAddress.sin_family      = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port        = htons (portNumber);

    // Tell the server to restart immediately after a previous shutdown,
    // even if it looks like the socket is still in use.
    // Otherwise, we might have to wait a little while before re-running the
    // server once it has stopped.
    const int optionValue = 1;
    setsockopt (serverSocket, SOL_SOCKET, SO_REUSEADDR, &optionValue, sizeof (int));

    int bindSuccess = bind (serverSocket, (struct sockaddr*)&serverAddress, sizeof (serverAddress));

    // If this assert fails, wait a short while to let the operating
    // system clear the port before trying again
    assert (bindSuccess >= 0);

    return serverSocket;
}

// Wait for a browser to request a connection,
// then return the socket on which the conversation will take place.
int waitForConnection (int serverSocket) {
    
    // Listen for a connection.
    const int serverMaxBacklog = 10;
    listen (serverSocket, serverMaxBacklog);
 
    // Accept the connection.
    struct sockaddr_in clientAddress;
    socklen_t clientLen = sizeof (clientAddress);
    int connectionSocket = accept (serverSocket, (struct sockaddr*)&clientAddress, &clientLen);
    
    // Check for a connection error.
    assert (connectionSocket >= 0);
 
    return connectionSocket;
}
