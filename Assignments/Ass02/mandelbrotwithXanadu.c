/*  
 *  Justin Ng & Jeff Zhang
 *  Tutor class is wed13-oboe
 *  Tutor is Steven Strijakov
 *  File first created on the 13/04/2016
 *  This program plots the Mandelbrot set and presents it as a BMP file.
 *  This done via the use of 256 iterations.
 *  It also acts as a web server to serve up the picture of the Mandelbrot set.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <netinet/in.h>
#include <unistd.h>
#include <math.h>

#include "pixelColor.h"
#include "mandelbrot.h" 
#include "pixelColor.c"

#define SIZE 512
#define MAXIMUM_ITERATIONS 256
#define LIMIT 4
#define BYTES_PER_PIXEL 3
#define BITS_PER_PIXEL (BYTES_PER_PIXEL*8)
#define NUMBER_PLANES 1
#define PIX_PER_METRE 2835
#define MAGIC_NUMBER 0x4d42
#define NO_COMPRESSION 0
#define OFFSET 54
#define DIB_HEADER_SIZE 40
#define NUM_COLORS 0
#define BLACK 0
#define WHITE 255

#define BREAD_SERVER_VERSION 2.0
#define REQUEST_BUFFER_SIZE 1000
#define DEFAULT_PORT 6969
#define NUMBER_OF_PAGES_TO_SERVE 9001

#define ZOOM zoom
#define CENTER_X x
#define CENTER_Y y
 
typedef unsigned char  bits8;
typedef unsigned short bits16;
typedef unsigned int   bits32;

typedef struct _RGB {
   int red;
   int green;
   int blue;
} RGB;
 
int escapeSteps(double x, double y);
void writeHeader (FILE *file);
int waitForConnection (int serverSocket);
int makeServerSocket (int portno);
void serveBread (int socket);
void serveBreadSlice (int socket, double x, double y, int zoom);

int main(int argc, char * argv[]) {
    double x = 0.0;
    double y = 0.0;
    int zoom = 0;

    int serverSocket = makeServerSocket(DEFAULT_PORT);
    char request[REQUEST_BUFFER_SIZE];
    int numberServed = 0;
    char requested[256];
    
    printf ("************************************\n");
    printf ("Starting Almondbread server %f\n", BREAD_SERVER_VERSION);
    printf ("Serving steaming hot almondbread since 2016\n");
    printf ("Access this server at http://localhost:%d/ if you're at uni\n", DEFAULT_PORT);
    printf ("Access this server at http://weill.cse.unsw.edu.au:%d/ if you're at home\n", DEFAULT_PORT);
    printf ("************************************\n");
    
    while (numberServed < NUMBER_OF_PAGES_TO_SERVE) {
        printf ("*** So far served %d pages ***\n", numberServed);
 
        // STEP 1. wait for a request to be sent from a web browser, 
        // then open a new connection for this conversation
        int connectionSocket = waitForConnection(serverSocket);
 
        // STEP 2. read the first line of the request
        int bytesRead = recv (connectionSocket, request, sizeof(request) - 1, 0);
        assert (bytesRead >= 0);
        // check that we were able to read some data from the connection
 
        // echo entire request to the console for debugging
        printf (" *** Received http request ***\n %s\n", request);
        
        // STEP 3. read in what the client requested
        sscanf(request, "GET %s HTTP/", requested);
 
        // STEP 4. serve client what they requested: root directory or specific BMP
        if (strcmp(requested, "/") == 0) {
            printf (" *** Sending bread ***\n");
            serveBread(connectionSocket);
        } else {
            printf (" *** Sending slice of bread ***\n");
            sscanf(requested, "/tile_x%lf_y%lf_z%d.bmp", &x, &y, &zoom);
            printf("requested x-coordinate is %lf\n", x);
            printf("requested y-coordinate is %lf\n", y);
            printf("requested zoom is %d\n", zoom);
            serveBreadSlice(connectionSocket, x, y, zoom);
        }
        
        // STEP 5. close the connection after sending the page- keep aust beautiful
        close (connectionSocket);
        ++numberServed;
    }

    return EXIT_SUCCESS;
}

int escapeSteps(double x, double y) {
    double real = 0;
    double newReal = 0;
    double im = 0;
    double newIm = 0;
    double mod = 0;
    int steps = 0; 
    while (steps < MAXIMUM_ITERATIONS && mod < LIMIT) {
        newReal = pow(real,2) - pow(im,2) + x;
        newIm = 2 * real * im + y;
        mod = pow(newReal,2) + pow(newIm,2);
        real = newReal;
        im = newIm;
        steps++;
    }
    return steps;
}

//Serve HTML tileviewer to web server
void serveBread (int socket) {
    char* message;

    //sends the http response
    message =
        "HTTP/1.0 200 Found\n"
        "Content-Type: text/html\r\n"
        "\r\n"
        "<html>\n"
        "   <head><title>Bread Server</title></head>\n"
        "</html>\n"
        "<!DOCTYPE html>\n"
        "<script src=\"//almondbread.cse.unsw.edu.au/tiles.js\"></script>"
        "\n";
   printf ("about to send=> %s\n", message);
   write (socket, message, strlen (message));
}

//Serve BMP to web server
void serveBreadSlice (int socket, double x, double y, int zoom) {
    char* message;

    // send the http response header
    message =
    "HTTP/1.0 200 OK\r\n"
    "Content-Type: image/bmp\r\n"
    "\r\n";
    printf ("about to send=> %s\n", message);
    write (socket, message, strlen (message));
   
    // now send the BMP

    // writes out the BMP header
    unsigned char bmp[] = {
        0x42,0x4d,0x5a,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x36,0x00,0x00,0x00,0x28,0x00,
        0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x02,
        0x00,0x00,0x01,0x00,0x18,0x00,0x00,0x00,
        0x00,0x00,0x24,0x00,0x00,0x00,0x13,0x0b,
        0x00,0x00,0x13,0x0b,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00};
    write (socket, bmp, sizeof(bmp));
    
    // writes out the actual pixels of the BMP
    double pixelDist = pow(2, -ZOOM);
    double xCurrent = CENTER_X - pixelDist*(SIZE/2);
    double yCurrent = CENTER_Y - pixelDist*(SIZE/2);
    double xMax = CENTER_X + pixelDist*(SIZE/2);
    double yMax = CENTER_Y + pixelDist*(SIZE/2);
    char RGB[3];
    double xInitial = xCurrent;

    while (yCurrent < yMax) {
        while (xCurrent < xMax) {
            RGB[0] = stepsToBlue (escapeSteps(xCurrent, yCurrent));
            RGB[1] = stepsToGreen (escapeSteps(xCurrent, yCurrent));
            RGB[2] = stepsToRed (escapeSteps(xCurrent, yCurrent));
            xCurrent = xCurrent + pixelDist;
            write (socket, RGB, sizeof(RGB));
        }
        yCurrent = yCurrent + pixelDist;
        xCurrent = xInitial;
   }
}

// start the server listening on the specified port number
int makeServerSocket (int portNumber) {
    // create socket
    int serverSocket = socket (AF_INET, SOCK_STREAM, 0);
    
    // check there was no error in opening the socket
    assert (serverSocket >= 0);

    // bind the socket to the listening port
    struct sockaddr_in serverAddress;
    serverAddress.sin_family      = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port        = htons (portNumber);

    // tell the server to restart immediately after a previous shutdown
    // even if it looks like the socket is still in use
    // otherwise we might have to wait a little while before rerunning the
    // server once it has stopped
    const int optionValue = 1;
    setsockopt (serverSocket, SOL_SOCKET, SO_REUSEADDR, &optionValue, sizeof (int));

    int bindSuccess = bind (serverSocket, (struct sockaddr*)&serverAddress, sizeof (serverAddress));

    // if this assert fails wait a short while to let the operating
    // system clear the port before trying again
    assert (bindSuccess >= 0);

    return serverSocket;
}

// Wait for a browser to request a connection,
// returns the socket on which the conversation will take place
int waitForConnection (int serverSocket) {
    
    // listen for a connection
    const int serverMaxBacklog = 10;
    listen (serverSocket, serverMaxBacklog);
 
    // accept the connection
    struct sockaddr_in clientAddress;
    socklen_t clientLen = sizeof (clientAddress);
    int connectionSocket = accept (serverSocket, (struct sockaddr*)&clientAddress, &clientLen);
    
    // check for connection error
    assert (connectionSocket >= 0);
 
    return connectionSocket;
}
