/*
 *  reverseList.c
 *  2013s1 Practice Prac Exam #1
 *  UNSW comp1917
 *
 *  Implemented by Justin Ng
 *  Share freely CC-BY-3.0
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "reverseList.h"

list reverse (list inputList) {
    if (inputList == NULL || inputList->rest == NULL) {
        return inputList;
    } else if (inputList->rest->rest == NULL) {
        list prev = inputList;
        list curr = inputList->rest;
        curr->rest = prev;
        prev->rest = NULL;
        return curr;
    } else {
        list prev = inputList;
        list curr = inputList->rest;
        list after = inputList->rest->rest;
        prev->rest = NULL;
        while (curr->rest != NULL) {
            curr->rest = prev;
            prev = curr;
            curr = after;
        }
        curr->rest = prev;
        return curr;
    }
}
