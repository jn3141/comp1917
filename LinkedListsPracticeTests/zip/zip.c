/*
 *  zip.c
 *
 *  2013 s1 Practice Prac Exam #2
 *  UNSW comp1917
 *
 *  Created by INSERT YOUR NAME HERE
 *  Share freely CC-BY-3.0
 *
 */

// Complete this function and submit this file ONLY
// for the prac exam.  Do not change the other supplied
// as you will not be submitting them.

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "zip.h"

list zip (list listA, list listB) {
    if (listA == NULL) {
        return listA;
    } else if (listA->rest == NULL) {
        listA->rest = listB;
        return listA;
    } else {
        list currA = listA;
        list nextA = listA->rest;
        list currB = listB;
        list nextB = listB->rest;
        while (currA->rest != NULL) {
            currA->rest = currB;
            currB->rest = nextA;
            currA = nextA;
            currB = nextB;
            nextA = nextA->rest;
            nextB = nextB->rest;
        }
        currA->rest = currB;
        return listA;
    }
}
