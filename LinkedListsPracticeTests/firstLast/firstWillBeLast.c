/*
 *  firstWillBeLast.c
 *  concept thanks to Matthew
 *  20 13 s1 Practice Prac Exam #2
 *  UNSW comp1917
 *
 *  Implemented by Justin Ng
 *  Share freely CC-BY-3.0
 *
 */

// Complete this function and submit this file ONLY
// for the prac exam.  Do not change the other supplied
// as you will not be submitting them.

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "firstWillBeLast.h"

nodePtr frontToBack (nodePtr list) {
    if (list == NULL || list->next == NULL) {
        return list;
    } else {
        nodePtr first = list;
        nodePtr newHead = list->next;
        nodePtr curr = list->next;

        first->next = NULL;

        while (curr->next != NULL) {
            curr = curr->next;
        }

        curr->next = first;
        return newHead;
    }
}
