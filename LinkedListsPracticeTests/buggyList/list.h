// list.h

typedef struct _list *list; 


typedef struct _node * link;

struct _node { 
    int value; 
    link next; 
}node;

struct _list { 
    link first;
};

//Function Prototypes 
list createList( void ); 
int length (list ls);
void addToEnd (list ls, int item);
int deleteFirstElement (list ls);
void printList (list ls);
