#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "list.h"

#define TEST_LIST_SIZE 10

int main (int argc, char *argv[]){
    int i;
    list ls1;


    printf("Test 1: Create list\n");
    ls1 = createList();

    assert(ls1 != NULL);

    assert(length(ls1) == 0);

    printf("Passed\n");

    printf("Test 2: addToEnd\n");
    i=0;
    while(i< TEST_LIST_SIZE){
        addToEnd(ls1,i);

        assert(length(ls1) == i+1);

        i++;
    }
    printf("Passed\n");
    printf("Test 3: deleteFirstElement\n");
    i=0;
    while(i< TEST_LIST_SIZE){
        int item = deleteFirstElement(ls1);
        assert(item == i);
        assert(length(ls1) == TEST_LIST_SIZE - i - 1);

        i++;
    }
    printf("Passed\n");

    printf("All tests passed! You suck!\n");
    return EXIT_SUCCESS;
}
