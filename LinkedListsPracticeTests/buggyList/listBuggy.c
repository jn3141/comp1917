// An originally buggy linked list implementation of a queue
// Debugged by Justin Ng

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "list.h"

static link createNode(int item){
    link n = malloc (sizeof(struct _node));
    assert(n != NULL);
    n->value = item;
    n->next = NULL;
    return n;
}

// Creates an empty list
list createList (void){
    list ls = malloc (sizeof (struct _list));
    assert(ls != NULL);
    ls->first = NULL;
    return ls;
}

// Creates an empty Queue
int length (list ls){
    link curr = ls->first;
    int len = 0;
    if (ls->first == NULL) {

    } else {
        while (curr->next != NULL){
            curr = curr->next;
            len++;
        }
        len++;
    }

    return len;
}


void addToEnd (list ls, int item){
    assert(ls != NULL);
    link n = createNode(item);
    link curr = ls->first;
    if(ls->first  == NULL){
        ls->first = n;
    } else {
        while(curr->next != NULL){
            curr = curr->next;
        }
        curr->next = n;
    }
}

//Returns the value from the deleted node
//Assume that this is only called if there is a node to delete
int deleteFirstElement (list ls){
    assert(ls != NULL);
    assert(ls->first != NULL);
    int item = ls->first->value;
    link delNode = ls->first;
    ls->first = ls->first->next;
    free(delNode);
    return item;
}

//Returns the value from the deleted node
void printList (list ls){
    assert(ls != NULL);

    link curr = ls->first;
    while(curr != NULL){
        printf("%d ",curr->value);
        curr = curr->next;
    }
    printf("\n");
}
