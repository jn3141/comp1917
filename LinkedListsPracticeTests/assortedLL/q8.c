// Credits to Riyasat Saber for the practice questions
// Implementation by Justin Ng

#include <stdlib.h>
#include <stdio.h>
#include "list.h"

#define TRUE 1
#define FALSE 0

/*
 *  Determines whether link b is a sublink of link a, that is, does
 *  link b exist inside link a. If a sublink is found, return the node
 *  in link a where the sublink is found, else return NULL.
 */
link sublink(link a, link b) {
    if (a == NULL || b == NULL) {
        link sub;
        sub = NULL;
        return sub;
    } else if (b->next == NULL) {
        if (a->data == b->data) {
            return a;
        } else {
            link sub;
            sub = NULL;
            return sub;
        }
    } else {
        link currentA = a;
        link currentB = b;
        int counterA = 1;
        int counterB = 1;
        while (currentA != NULL) {
            currentA = currentA->next;
            counterA++;
        }
        while (currentB != NULL) {
            currentB = currentB->next;
            counterB++;
        }
        currentA = a;
        currentB = b;
        if (counterA < counterB) {
            link subStart;
            subStart = NULL;
            return subStart;
        } else {
            link subStart;
            while (currentA != NULL) {
                if (currentA->data == currentB->data) {
                    subStart = currentA;
                    break;
                } else {
                    subStart = NULL;
                    currentA = currentA->next;
                }
            }
            int diverged = FALSE;
            if (subStart != NULL) {
                while (currentB != NULL) {
                    if (currentB->data != currentA->data) {
                        diverged = TRUE;
                        break;
                    } else {
                        currentB = currentB->next;
                        currentA = currentA->next;
                    }
                }
                if (diverged == FALSE) {
                    return subStart;
                } else {
                    subStart = NULL;
                    return subStart;
                }
            } else {
                return subStart;
            }
        }
    }
}
