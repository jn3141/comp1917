// Credits to Riyasat Saber for the practice questions
// Implementation by Justin Ng

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "list.h"

link newReverseList(link head) {
    link reverse = newNode(0);
    link currentReverse = reverse;
    if (head != NULL) {
        if (head->next == NULL) {
            currentReverse->data = head->data;
        } else {
            link currentInput = head;
            int counter = 0;
            while (currentInput->next != NULL) {
                currentInput = currentInput->next;
                counter++;
            }

            int data[counter];
            int inputCounter = counter;
            currentInput = head;
            while (inputCounter != -1) {
                data[inputCounter] = currentInput->data;
                currentInput = currentInput->next;
                inputCounter--;
            }

            int reverseCounter = 0;
            while (reverseCounter != counter) {
                currentReverse->data = data[reverseCounter];
                currentReverse->next = newNode(0);
                currentReverse = currentReverse->next;
                reverseCounter++;
            }
            currentReverse->data = data[reverseCounter];
        }
    } else {
        return NULL;
    }
    return reverse;
}
