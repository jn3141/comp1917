// Tests File
// Write all of your tests in here

// Any questions email riyasat.saber@student.unsw.edu.au
// but ask your tutor first

// compile with gcc -Wall -Werror -O -o tests tests.c questions.c  list.c
#include <stdio.h>
#include "list.h"
#include <assert.h>

int main(){
	printf("-----------Tests for Q8-----------\n");
	//example of how to make a linked list quickly and test a question
	int keys1[] = {1,2,3,4,5,5,6,7,9,10};
	link list1 = createListWithKeys(keys1, sizeof(keys1)/sizeof(int));
	int keys2[] = {2,3,4};
	link list2 = createListWithKeys(keys2, sizeof(keys2)/sizeof(int));

	printf("listA: ");
	printList(list1);
	printf("listB: ");
	printList(list2);

	link sl = sublink(list1,list2);
	printf("sublink: ");
	printList(sl);
	freeList(list1);
	freeList(list2);
	return 0;
}