// Tests File
// Write all of your tests in here

// Any questions email riyasat.saber@student.unsw.edu.au
// but ask your tutor first

// compile with gcc -Wall -Werror -O -o tests tests.c questions.c  list.c
#include <stdio.h>
#include "list.h"
#include <assert.h>

int main(){
	printf("-----------Tests for Q3-----------\n");
	//example of how to make a linked list quickly and test a question
	int keys[] = {1,2,3,4,5,6,7,8,9};
	link list1 = createListWithKeys(keys, sizeof(keys)/sizeof(int));
	printf("Original List: ");
	printList(list1);
	printf("Reverse List: ");
	printList(newReverseList(list1));
	freeList(list1);
	return 0;
}
