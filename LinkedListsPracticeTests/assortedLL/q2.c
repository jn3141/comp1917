// Credits to Riyasat Saber for the practice questions
// Implementation by Justin Ng

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "list.h"

// Given a linked list and an integer n, write a function that returns
// the n’th node from the end of the linked list. The very last node is
// when n == 0
link nodeFromEnd(link head, int n) {
    int counter = 0;
    link returnNode = head;
    if (head == NULL) {

    } else {
        link current = head;
        while (current->next != NULL) {
            current = current->next;
            counter++;
        }

        if (n <= counter) {
            counter = counter - n;
            current = head;
            while (counter != 0) {
                counter--;
                current = current->next;
            }
            returnNode = current;
        }
    }
    return returnNode;
}
