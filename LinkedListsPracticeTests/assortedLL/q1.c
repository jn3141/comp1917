// Credits to Riyasat Saber for the practice questions
// Implementation by Justin Ng

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "list.h"

// Given a linked list and an integer n, write a function that computes
// and returns the sum of every n node. If n = 0, return -1.
int sumEveryN(link head, int n) {
    int counter = 1;
    int calc = 0;

    if (n == 0) {
        calc = -1;
    } else {
        if (head == NULL) {
            calc = -1;
        } else {
            link current = head;
            while (current != NULL) {
                if (counter % n == 0) {
                    calc += current->data;
                }
                counter++;
                current = current->next;
            }
        }
    }
    return calc;
}
