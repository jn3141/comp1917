// Credits to Riyasat Saber for the practice questions
// Implementation by Justin Ng

#include <stdio.h>
#include <stdlib.h>
#include "list.h"

#define TRUE 1
#define FALSE 0

link newReverseList(link head) {
    link reverse = newNode(0);
    link currentReverse = reverse;
    if (head != NULL) {
        if (head->next == NULL) {
            currentReverse->data = head->data;
        } else {
            link currentInput = head;
            int counter = 0;
            while (currentInput->next != NULL) {
                currentInput = currentInput->next;
                counter++;
            }

            int data[counter];
            int inputCounter = counter;
            currentInput = head;
            while (inputCounter != -1) {
                data[inputCounter] = currentInput->data;
                currentInput = currentInput->next;
                inputCounter--;
            }

            int reverseCounter = 0;
            while (reverseCounter != counter) {
                currentReverse->data = data[reverseCounter];
                currentReverse->next = newNode(0);
                currentReverse = currentReverse->next;
                reverseCounter++;
            }
            currentReverse->data = data[reverseCounter];
        }
    } else {
        return NULL;
    }
    return reverse;
}

int isPalindrome (link head) {
    int palindrome = FALSE;

    if (head == NULL || head->next == NULL) {
        palindrome = TRUE;
    } else if (head->next->next == NULL) {
        if (head->data == head->next->data) {
            palindrome = TRUE;
        }
    } else {
        link reverseHead = newReverseList(head);
        link current = head;
        link currentReverse = reverseHead;
        while (current != NULL) {
            if (current->data != currentReverse->data) {
                palindrome = FALSE;
                break;
            } else {
                palindrome = TRUE;
                current = current->next;
                currentReverse = currentReverse->next;
            }
        }
    }
    return palindrome;
}
