// Tests File
// Write all of your tests in here

// Any questions email riyasat.saber@student.unsw.edu.au
// but ask your tutor first

// compile with gcc -Wall -Werror -O -o tests tests.c questions.c  list.c
#include <stdio.h>
#include "list.h"
#include <assert.h>

int main(){
	printf("-----------Tests for Q2-----------\n");
	//example of how to make a linked list quickly and test a question
	int keys1[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
	link list1 = createListWithKeys(keys1, sizeof(keys1)/sizeof(int));

	printList(list1);
	printf("The %d node from the end is ... %d!\n", 0, nodeFromEnd(list1,0)->data);
    printf("The %d node from the end is ... %d!\n", 1, nodeFromEnd(list1,1)->data);
    printf("The %d node from the end is ... %d!\n", 2, nodeFromEnd(list1,2)->data);
    printf("The %d node from the end is ... %d!\n", 3, nodeFromEnd(list1,3)->data);
    printf("The %d node from the end is ... %d!\n", 4, nodeFromEnd(list1,4)->data);
    printf("The %d node from the end is ... %d!\n", 5, nodeFromEnd(list1,5)->data);
    printf("The %d node from the end is ... %d!\n", 6, nodeFromEnd(list1,6)->data);
    printf("The %d node from the end is ... %d!\n", 7, nodeFromEnd(list1,7)->data);
    printf("The %d node from the end is ... %d!\n", 8, nodeFromEnd(list1,8)->data);
    printf("The %d node from the end is ... %d!\n", 9, nodeFromEnd(list1,9)->data);

	freeList(list1);
	return 0;
}
