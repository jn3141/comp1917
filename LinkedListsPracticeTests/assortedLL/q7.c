// Credits to Riyasat Saber for the practice questions
// Implementation by Justin Ng

#include <stdlib.h>
#include <stdio.h>
#include "list.h"

/*
 *  Creates a new linked list from elements that exist in both list a
 *  and list b.
 */
link intersection(link a, link b) {
    if (a == NULL || b == NULL) {
        link intersect = newNode(0);
        return intersect;
    } else if (a->next == NULL && b->next == NULL) {
        link intersect;
        if (a->data == b->data) {
            intersect = newNode(a->data);
        } else {
            intersect = NULL;
        }
        return intersect;
    } else {
        link currentA = a;
        link currentB = b;
        link intersect = newNode(0);
        link previousIntersect = intersect;
        link currentIntersect = intersect;
        while (currentA->next != NULL) {
            while (currentB->next != NULL) {
                if (currentA->data == currentB->data) {
                    currentIntersect->data = currentA->data;
                    currentIntersect->next = newNode(0);
                    previousIntersect = currentIntersect;
                    currentIntersect = currentIntersect->next;
                }
                currentB = currentB->next;
            }
            if (currentA->data == currentB->data) {
                currentIntersect->data = currentA->data;
                currentIntersect->next = newNode(0);
                previousIntersect = currentIntersect;
                currentIntersect = currentIntersect->next;
            }
            currentB = b;
            currentA = currentA->next;
        }
        while (currentB->next != NULL) {
            if (currentA->data == currentB->data) {
                currentIntersect->data = currentA->data;
                currentIntersect->next = newNode(0);
                previousIntersect = currentIntersect;
                currentIntersect = currentIntersect->next;
            }
            currentB = currentB->next;
        }
        if (currentA->data == currentB->data) {
            currentIntersect->data = currentA->data;
            currentIntersect->next = NULL;
        } else {
            previousIntersect->next = NULL;
        }
        return intersect;
    }
}
