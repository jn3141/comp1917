// Tests File
// Write all of your tests in here

// Any questions email riyasat.saber@student.unsw.edu.au
// but ask your tutor first

// compile with gcc -Wall -Werror -O -o tests tests.c questions.c  list.c
#include <stdio.h>
#include "list.h"
#include <assert.h>

int main(){
	printf("-----------Tests for Q4-----------\n");
	//example of how to make a linked list quickly and test a question
	int keys[] = {8,2,1};
	link list1 = createListWithKeys(keys, sizeof(keys)/sizeof(int));
	printf("Original List: ");
	printList(list1);
	printf("The order of the list: %d\n",findOrder(list1));
	freeList(list1);
	return 0;
}
