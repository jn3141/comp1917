// Tests File
// Write all of your tests in here

// Any questions email riyasat.saber@student.unsw.edu.au
// but ask your tutor first

// compile with gcc -Wall -Werror -O -o tests tests.c questions.c  list.c
#include <stdio.h>
#include "list.h"
#include <assert.h>

int main(){
	printf("-----------Tests for Q9-----------\n");
	int keys1[] = {1,2,3,4,5,5,6,7,9,10};
	link list1 = createListWithKeys(keys1, sizeof(keys1)/sizeof(int));
	int keys2[] = {5,6,5};
	link list2 = createListWithKeys(keys2, sizeof(keys2)/sizeof(int));
	printf("ListA: ");
	printList(list1);
	printf("ListB: ");
	printList(list2);
	if(isPalindrome(list1)) printf("ListA is a Palindrome!\n");
	else printf("ListA is not a Palindrome!\n");

	if(isPalindrome(list2)) printf("ListB is a Palindrome!\n");
	else printf("ListB is not a Palindrome!\n");
		
	freeList(list1);
	freeList(list2);
	return 0;
}