// Tests File
// Write all of your tests in here

// Any questions email riyasat.saber@student.unsw.edu.au
// but ask your tutor first

// compile with gcc -Wall -Werror -O -o tests tests.c questions.c  list.c
#include <stdio.h>
#include "list.h"
#include <assert.h>

int main(){
	printf("-----------Tests for Q7-----------\n");
	//example of how to make a linked list quickly and test a question
	int keys1[] = {1,3,5,7};
	link list1 = createListWithKeys(keys1, sizeof(keys1)/sizeof(int));
	int keys2[] = {1,9,3,11,5,7};
	link list2 = createListWithKeys(keys2, sizeof(keys2)/sizeof(int));

	printf("listA: ");
	printList(list1);
	printf("listB: ");
	printList(list2);

	link linkIntersection = intersection(list1,list2);
	printf("intersection: ");
	printList(linkIntersection);

	freeList(list1);
	freeList(list2);
	freeList(linkIntersection);
	return 0;
}
