// Credits to Riyasat Saber for the practice questions
// Implementation by Justin Ng

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "list.h"

#define ASCEND 1
#define DESCEND 0
#define NEITHER -1
#define TRUE 1
#define FALSE 0

int findOrder(link head) {
    int order = NEITHER;
    if (head != NULL) {
        int counter = 0;
        link current = head;

        while (current->next != NULL) {
            current = current->next;
            counter++;
        }
        counter++;

        if (counter == 1) {
            order = NEITHER;
        } else if (counter == 2) {
            int diff = head->next->data - head->data;
            if (diff < 0) {
                order = DESCEND;
            } else if (diff >= 0) {
                order = ASCEND;
            }
        } else if (counter >= 3) {
            int hadNeg = FALSE;
            int hadPos = FALSE;
            current = head;
            link previous = head;
            while (current->next != NULL) {
                previous = current;
                current = current->next;
                if (current->data - previous->data < 0) {
                    hadNeg = TRUE;
                } else if (current->data - previous->data >=0) {
                    hadPos = TRUE;
                }
            }
            if (hadNeg == TRUE && hadPos == TRUE) {
                order = NEITHER;
            } else if (hadNeg == TRUE && hadPos == FALSE) {
                order = DESCEND;
            } else if (hadNeg == FALSE && hadPos == TRUE) {
                order = ASCEND;
            }
        }
    } else {
        order = NEITHER;
    }
    return order;
}
