// Credits to Riyasat Saber for the practice questions
// Implementation by Justin Ng

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "list.h"

link deleteEveryNNode(link head, int n) {
    int counter = 1;
    int numItems = 1;
    if (head == NULL) {
        numItems = 0;
    } else {
        link numFinder = head;
        while (numFinder->next != NULL) {
            numItems++;
            numFinder = numFinder->next;
        }
    }

    if (n <= numItems) {
        if (head != NULL && head->next != NULL) {
            link current = head;
            link previous = head;
            while (current != NULL) {
                if (counter % n == 0) {
                    previous->next = current->next;
                    current = current->next;
                } else {
                    previous = current;
                    current = current->next;
                }
                counter++;
            }
            previous->next = NULL;
        }
    }
    return head;
}
