// Tests File
// Write all of your tests in here

// Any questions email riyasat.saber@student.unsw.edu.au
// but ask your tutor first

// compile with gcc -Wall -Werror -O -o tests tests.c questions.c  list.c
#include <stdio.h>
#include "list.h"
#include <assert.h>

int main(){
	printf("-----------Tests for Q5-----------\n");
	//example of how to make a linked list quickly and test a question
	int keys[] = {1,2,3,4,5,6,7,8,9,10,11};
	link list1 = createListWithKeys(keys, sizeof(keys)/sizeof(int));
    link list2 = createListWithKeys(keys, sizeof(keys)/sizeof(int));
    link list3 = createListWithKeys(keys, sizeof(keys)/sizeof(int));
    link list4 = createListWithKeys(keys, sizeof(keys)/sizeof(int));
	printf("Original List: ");
	printList(list1);
	printf("after deleting every %d node, the list is:\n",2);
	printList(deleteEveryNNode(list1, 2));
    printf("after deleting every %d node, the list is:\n",11);
    printList(deleteEveryNNode(list2, 11));
    printf("after deleting every %d node, the list is:\n",12);
    printList(deleteEveryNNode(list3, 12));
    printf("after deleting every %d node, the list is:\n",5);
    printList(deleteEveryNNode(list4, 5));
	freeList(list1);
	return 0;
}
