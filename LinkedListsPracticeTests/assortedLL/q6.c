// Credits to Riyasat Saber for the practice questions
// Implementation by Justin Ng

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "list.h"

link zip(link a, link b) {
    link zipped = malloc(sizeof(struct _link));
    link currentZip = zipped;
    link currentA = a;
    link currentB = b;
    int lengthA = 1;
    int lengthB = 1;
    if (a != NULL) {
        while (currentA->next != NULL) {
            currentA = currentA->next;
            lengthA++;
        }
    }
    if (b != NULL) {
        while (currentB->next != NULL) {
            currentB = currentB->next;
            lengthB++;
        }
    }

    currentA = a;
    currentB = b;
    if (a != NULL && b != NULL) {
        if (lengthA > lengthB) {
            while (currentA->next != NULL) {
                currentZip->data = currentA->data;
                currentZip->next = malloc(sizeof(struct _link));
                currentZip = currentZip->next;
                currentZip->data = currentB->data;
                currentZip->next = malloc(sizeof(struct _link));
                currentZip = currentZip->next;
                currentA = currentA->next;
                currentB = currentB->next;
            }

            while (currentA->next != NULL) {
                printf("5");
                currentZip->data = currentA->data;
                currentZip->next = malloc(sizeof(struct _link));
                currentZip = currentZip->next;
                currentA = currentA->next;
            }
            currentZip->data = currentA->data;
        } else if (lengthB > lengthA) {
            while (currentA->next != NULL) {
                currentZip->data = currentA->data;
                currentZip->next = malloc(sizeof(struct _link));
                currentZip = currentZip->next;
                currentZip->data = currentB->data;
                currentZip->next = malloc(sizeof(struct _link));
                currentZip = currentZip->next;
                currentA = currentA->next;
                currentB = currentB->next;
            }

            currentZip->data = currentA->data;
            currentZip->next = malloc(sizeof(struct _link));
            currentZip = currentZip->next;

            while (currentB->next != NULL) {
                currentZip->data = currentB->data;
                currentZip->next = malloc(sizeof(struct _link));
                currentZip = currentZip->next;
                currentB = currentB->next;
            }
            currentZip->data = currentB->data;
        } else if (lengthB == lengthA) {
            while (currentA->next != NULL) {
                currentZip->data = currentA->data;
                currentZip->next = malloc(sizeof(struct _link));
                currentZip = currentZip->next;
                currentZip->data = currentB->data;
                currentZip->next = malloc(sizeof(struct _link));
                currentZip = currentZip->next;
                currentA = currentA->next;
                currentB = currentB->next;
            }
            currentZip->data = currentA->data;
            currentZip->next = malloc(sizeof(struct _link));
            currentZip = currentZip->next;
            currentZip->data = currentB->data;
        }
    }
    return zipped;
}
